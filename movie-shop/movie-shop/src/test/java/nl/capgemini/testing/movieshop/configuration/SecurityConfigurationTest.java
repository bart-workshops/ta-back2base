package nl.capgemini.testing.movieshop.configuration;

import nl.capgemini.testing.movieshop.model.users.User;
import nl.capgemini.testing.movieshop.repositories.UserRepository;
import nl.capgemini.testing.movieshop.services.users.UserManagementService;
import nl.capgemini.testing.movieshop.services.users.UserManagementServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationContext;
import org.springframework.expression.Expression;
import org.springframework.expression.ExpressionParser;
import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.expression.SecurityExpressionHandler;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.ProviderManager;
import org.springframework.security.config.annotation.ObjectPostProcessor;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractInterceptUrlConfigurer;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.annotation.web.configurers.SessionManagementConfigurer;
import org.springframework.security.config.core.GrantedAuthorityDefaults;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.authentication.session.SessionAuthenticationStrategy;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SuppressWarnings("unchecked")
@ExtendWith(MockitoExtension.class)
class SecurityConfigurationTest {

    @Spy
    private final Map<Class<?>, Object> sharedObjectSpy = new HashMap<>();
    @InjectMocks
    private SecurityConfiguration testable;
    private HttpSecurity httpSecurityStub;
    @Mock
    private ObjectPostProcessor<Object> objectPostProcessorMock;
    @Mock
    private SessionAuthenticationStrategy sessionAuthenticationStrategyMock;

    @Mock
    private SecurityExpressionHandler<FilterInvocation> expressionHandlerMock;

    @Mock
    private ExpressionParser expressionParserMock;

    @Mock
    private ProviderManager authenticationManagerMock;

    @Mock
    private AccessDecisionManager accessDecisionManagerMock;

    @Mock
    private Expression expressionMock;
    private AuthenticationManagerBuilder authenticationBuilderStub;

    @Mock
    private UserRepository repositoryMock;

    @Mock
    private HttpServletRequest requestMock;
    @Mock
    private HttpServletResponse responseMock;
    @Mock
    private Authentication authenticationMock;
    @Mock
    private HttpSession sessionMock;
    @Mock
    private ApplicationContext applicationContextMock;

    @BeforeEach
    void setUp() {
        authenticationBuilderStub = new AuthenticationManagerBuilder(objectPostProcessorMock) {

            @Override
            protected ProviderManager performBuild() {
                return authenticationManagerMock;
            }

        };

        httpSecurityStub = new HttpSecurity(objectPostProcessorMock, authenticationBuilderStub, sharedObjectSpy);
        httpSecurityStub.setSharedObject(ApplicationContext.class, applicationContextMock);
    }

    @Test
    void should_setup_http_security_context() throws Exception {
        sharedObjectSpy.put(AuthenticationManager.class, authenticationManagerMock);

        when(expressionHandlerMock.getExpressionParser()).thenReturn(expressionParserMock);
        when(objectPostProcessorMock.postProcess(any())).thenAnswer(i -> i.getArgument(0));
        when(applicationContextMock.getBeanNamesForType(GrantedAuthorityDefaults.class)).thenReturn(new String[0]);

        when(accessDecisionManagerMock.supports(FilterInvocation.class)).thenReturn(true);
        when(accessDecisionManagerMock.supports(isA(ConfigAttribute.class))).thenReturn(true);
        when(expressionParserMock.parseExpression(isA(String.class))).thenReturn(expressionMock);
        when(requestMock.getSession(false)).thenReturn(sessionMock);
        when(requestMock.getSession()).thenReturn(sessionMock);

        testable.configure(httpSecurityStub);

        setField("sessionAuthenticationStrategy",
                SessionManagementConfigurer.class,
                httpSecurityStub.getConfigurer(SessionManagementConfigurer.class),
                sessionAuthenticationStrategyMock);

        setField("expressionHandler",
                ExpressionUrlAuthorizationConfigurer.class,
                httpSecurityStub.getConfigurer(ExpressionUrlAuthorizationConfigurer.class),
                expressionHandlerMock);

        setField("accessDecisionManager",
                AbstractInterceptUrlConfigurer.class,
                httpSecurityStub.getConfigurer(ExpressionUrlAuthorizationConfigurer.class),
                accessDecisionManagerMock);

        final DefaultSecurityFilterChain result = httpSecurityStub.build();
        final AbstractAuthenticationProcessingFilter resultAuthenticationFilter =
                (AbstractAuthenticationProcessingFilter) result.getFilters().stream()
                        .filter(f -> f instanceof AbstractAuthenticationProcessingFilter)
                        .findFirst()
                        .orElse(null);
        final SavedRequestAwareAuthenticationSuccessHandler handler = getField("successHandler",
                AbstractAuthenticationProcessingFilter.class,
                resultAuthenticationFilter);
        handler.onAuthenticationSuccess(requestMock, responseMock, authenticationMock);

        assertNotNull(result);
        verify(expressionHandlerMock).getExpressionParser();
        verify(objectPostProcessorMock, times(8)).postProcess(any());
        verify(repositoryMock).save(isA(User.class));

        verify(accessDecisionManagerMock).supports(FilterInvocation.class);
        verify(accessDecisionManagerMock, times(7)).supports(isA(ConfigAttribute.class));
        verify(expressionParserMock, times(7)).parseExpression(isA(String.class));
        verify(sessionMock).setAttribute("userDetails", null);
        verify(sessionMock).setAttribute(UserManagementService.SESSION_ROLE_KEY, null);
    }

    @Test
    void should_configure_authenticationBuilder() throws Exception {

        testable.configure(authenticationBuilderStub);

        assertEquals(UserManagementServiceImpl.class,
                authenticationBuilderStub.getDefaultUserDetailsService().getClass());
    }

    @Test
    void should_provide_a_password_encoder() {

        final PasswordEncoder result = testable.passwordEncoder();

        assertEquals(BCryptPasswordEncoder.class, result.getClass());
    }

    @Test
    void should_provide_an_user_management_service() {

        final UserManagementService result = testable.userManagementService();

        assertEquals(UserManagementServiceImpl.class, result.getClass());
    }

    private void setField(final String fieldName, final Class<?> declaringType, final Object object,
                          final Object value) throws IllegalAccessException, NoSuchFieldException {
        final Field field = declaringType.getDeclaredField(fieldName);
        field.setAccessible(true);
        field.set(object, value);
    }

    private <T> T getField(final String fieldName, final Class<?> declaringType, final Object object)
            throws IllegalAccessException, NoSuchFieldException {
        final Field field = declaringType.getDeclaredField(fieldName);
        field.setAccessible(true);
        return (T) field.get(object);
    }
}
