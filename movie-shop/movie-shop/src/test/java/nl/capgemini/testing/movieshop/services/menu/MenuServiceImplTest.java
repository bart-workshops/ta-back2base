package nl.capgemini.testing.movieshop.services.menu;

import nl.capgemini.testing.movieshop.controllers.generic.ExpandedMenuItem;
import nl.capgemini.testing.movieshop.model.menu.MenuItem;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MenuServiceImplTest {

    @InjectMocks
    private MenuServiceImpl testable;

    @Mock
    private MenuAware menuAwareMock;
    @Mock
    private ApplicationContext applicationContextMock;
    private MenuItem menuItemStub = new MenuItem("label", "url");

    @Test
    void should_fill_menu() {
        final Map<String, MenuAware> menuAwareBeans = new LinkedHashMap<>();
        menuAwareBeans.put("first", menuAwareMock);
        when(applicationContextMock.getBeansOfType(MenuAware.class)).thenReturn(menuAwareBeans);
        when(menuAwareMock.getMenuItem()).thenReturn(menuItemStub);

        testable.fillMenuComponents(new ContextRefreshedEvent(applicationContextMock));

        final List<ExpandedMenuItem> result = testable.fillMenu(menuAwareMock);

        assertEquals(menuItemStub.getLabel(), result.get(0).getLabel());
        assertEquals(menuItemStub.getUrl(), result.get(0).getUrl());
        assertTrue(result.get(0).isActive());
        verify(menuAwareMock).getMenuItem();
    }
}
