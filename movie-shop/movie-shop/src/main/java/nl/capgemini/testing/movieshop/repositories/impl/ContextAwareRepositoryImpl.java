package nl.capgemini.testing.movieshop.repositories.impl;

import lombok.RequiredArgsConstructor;
import nl.capgemini.testing.movieshop.repositories.ContextAwareRepository;
import nl.capgemini.testing.movieshop.repositories.Filter;
import nl.capgemini.testing.movieshop.repositories.SearchContext;
import nl.capgemini.testing.movieshop.repositories.SearchDataHolder;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;
import java.util.stream.IntStream;

@Component
@RequiredArgsConstructor
public class ContextAwareRepositoryImpl<T> implements ContextAwareRepository<T> {

    private final EntityManager entityManager;

    @SuppressWarnings("unchecked")
    @Override
    public List<T> findByContext(final SearchContext context) {
        return doCreateQuery((SearchDataHolder) context).getResultList();
    }

    public Query doCreateQuery(final SearchDataHolder context) {
        final String sqlStatement = createSqlStatement(context);
        final Query query = entityManager.createQuery(sqlStatement);
        applyLimitAndOffset(context, query);
        context.getParameters().forEach(query::setParameter);
        return query;
    }

    private void applyLimitAndOffset(final SearchDataHolder context, final Query query) {
        if (context.getOffSet() != null) {
            query.setFirstResult(context.getOffSet());
        }
        if (context.getLimit() != null) {
            query.setMaxResults(context.getLimit());
        }
    }

    private String createSqlStatement(final SearchDataHolder context) {
        final StringBuilder result = new StringBuilder();
        IntStream.range(0, context.getFilters().size())
                .forEach(index -> applyFilter(index, context, result));
        if (result.length() != 0) {
            result.insert(0, " WHERE ");
        }
        result.insert(0, "SELECT c FROM Product c");
        return result.toString().trim();
    }

    private void applyFilter(final int index, final SearchDataHolder searchContext, final StringBuilder result) {
        final Filter filter = searchContext.getFilters().get(index);
        if (result.length() > 0) {
            result.append("AND ");
        }
        result.append(filter.createQueryClause("param" + index, searchContext)).append(" ");
    }

}
