package nl.capgemini.testing.movieshop.services.pages;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.capgemini.testing.movieshop.configuration.ClassPathReader;
import nl.capgemini.testing.movieshop.model.pages.Page;
import nl.capgemini.testing.movieshop.repositories.PageRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class PageServiceImpl implements PageService {

    private final PageRepository repository;
    private final ClassPathReader pageReader = new ClassPathReader()
            .withBasePath("/pages/")
            .withFailedToRead((path, e) -> {
                log.warn("Page {} not found", path);
                throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
            });

    @Override
    public void createPage(final Page page) {
        repository.save(page);
    }

    @Override
    public Optional<Page> findPage(final String normalizedUri) {
        return repository.findByUrl(normalizedUri);
    }

    @Override
    public String retrieveBody(final Page page) {
        return pageReader.readString(page.getUrl());
    }

}
