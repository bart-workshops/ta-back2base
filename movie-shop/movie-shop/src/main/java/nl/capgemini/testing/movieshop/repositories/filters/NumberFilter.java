package nl.capgemini.testing.movieshop.repositories.filters;

import lombok.AccessLevel;
import lombok.Getter;
import nl.capgemini.testing.movieshop.repositories.Filter;
import nl.capgemini.testing.movieshop.repositories.SearchDataHolder;

public class NumberFilter<T extends Number> extends Filter {

    private final NumberFilter.ComparisonOperator operator;
    private final T lowerParam;
    private final T upperParam;

    public NumberFilter(final String fieldName, final T parameter, final ComparisonOperator operator) {
        super(fieldName);
        if (operator == ComparisonOperator.BETWEEN) {
            throw new IllegalArgumentException("Between may not be used here");
        }
        this.operator = operator;
        this.lowerParam = parameter;
        this.upperParam = null;
    }

    public NumberFilter(final String fieldName, final T lowerParam, final T upperParam) {
        super(fieldName);
        this.lowerParam = lowerParam;
        this.upperParam = upperParam;
        this.operator = ComparisonOperator.BETWEEN;
    }

    @Override
    public String createQueryClause(final String parameterName, final SearchDataHolder context) {
        if (operator == ComparisonOperator.BETWEEN) {
            return betweenQueryClause(parameterName, context);
        } else {
            return otherQueryClause(parameterName, context);
        }
    }

    private String betweenQueryClause(final String parameterName, final SearchDataHolder context) {
        context.getParameters().put(parameterName + "u", upperParam);
        return otherQueryClause(parameterName, context) + " AND :" + parameterName + "u";
    }

    private String otherQueryClause(final String parameterName, final SearchDataHolder context) {
        context.getParameters().put(parameterName + "l", lowerParam);
        return "c." + fieldName + " " + operator.symbol + " :" + parameterName + "l";
    }

    @Getter(AccessLevel.PACKAGE)
    public enum ComparisonOperator {
        EQUALS("="),
        LESS_THAN("<"),
        GREATER_THAN(">"),
        LESS_THAN_EQUAL_TO("<="),
        GREATER_THAN_EQUAL_TO(">="),
        BETWEEN("BETWEEN");

        private final String symbol;

        ComparisonOperator(final String symbol) {
            this.symbol = symbol;
        }

    }

}
