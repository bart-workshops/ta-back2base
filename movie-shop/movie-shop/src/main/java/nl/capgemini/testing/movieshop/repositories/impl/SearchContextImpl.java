package nl.capgemini.testing.movieshop.repositories.impl;

import lombok.Getter;
import nl.capgemini.testing.movieshop.controllers.validators.ValidationResult;
import nl.capgemini.testing.movieshop.repositories.Filter;
import nl.capgemini.testing.movieshop.repositories.SearchContext;
import nl.capgemini.testing.movieshop.repositories.SearchDataHolder;
import nl.capgemini.testing.movieshop.repositories.filters.NumberFilter;
import nl.capgemini.testing.movieshop.repositories.filters.StringFilter;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * This class contains the stateful context by which the search engine operates for a single search. It will contain
 * a number of filters, which can also be created using the provided functions in a builder like pattern.
 */
@Getter
public class SearchContextImpl implements SearchContext, SearchDataHolder {

    private static final char ESCAPE_CHARACTER = '\\';
    private final Map<String, Object> parameters = new HashMap<>();
    private final List<Filter> filters = new ArrayList<>();
    private Integer offSet;
    private Integer limit;

    @Override
    public SearchContext withOffSet(final ValidationResult<Integer> offSet) {
        if (offSet.isValid()) {
            this.offSet = offSet.getValue();
        }
        return this;
    }

    @Override
    public SearchContext withLimit(final ValidationResult<Integer> limit) {
        if (limit.isValid()) {
            this.limit = limit.getValue();
        }
        return this;
    }

    @Override
    public SearchContext withTitle(final ValidationResult<String> searchTitle) {
        if (searchTitle.isValid() && StringUtils.hasText(searchTitle.getValue())) {
            filters.add(new StringFilter("title", searchTitle.getValue(), false,
                    StringFilter.ComparisonOperator.CONTAINS));
        }
        return this;
    }

    @Override
    public SearchContext withId(final ValidationResult<Long> searchId) {
        if (searchId.isValid() && searchId.getValue() != null) {
            filters.add(new NumberFilter<>("id", searchId.getValue(), NumberFilter.ComparisonOperator.EQUALS));
        }
        return this;
    }

    @Override
    public SearchContext withYearBefore(final ValidationResult<Integer> yearBefore) {
        if (yearBefore.isValid() && yearBefore.getValue() != null) {
            filters.add(new NumberFilter<>("releaseYear", yearBefore.getValue(),
                    NumberFilter.ComparisonOperator.LESS_THAN));
        }
        return this;
    }

    @Override
    public SearchContext withYearAfter(final ValidationResult<Integer> yearAfter) {
        if (yearAfter.isValid() && yearAfter.getValue() != null) {
            filters.add(new NumberFilter<>("releaseYear", yearAfter.getValue(),
                    NumberFilter.ComparisonOperator.GREATER_THAN));
        }
        return this;
    }

    @Override
    public boolean hasFilters() {
        return !filters.isEmpty();
    }

    @Override
    public char getEscapeCharacter() {
        return ESCAPE_CHARACTER;
    }

}
