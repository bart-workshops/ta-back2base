package nl.capgemini.testing.movieshop.services.users;

import nl.capgemini.testing.movieshop.model.users.User;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Optional;

public interface UserManagementService {

    String SESSION_ROLE_KEY = "ROLE";

    UserRoles getRole(HttpSession session);

    boolean hasRoleSession(HttpSession session, UserRoles... roles);

    void registerFirstUser(String username, String password);

    List<User> retrieveAllUsers();

    Optional<User> findSingleUser(String username);

    void updateUser(User user);

    Optional<User> createNewUser(String username, final String password);

    void updatePassword(User user, String password);

}
