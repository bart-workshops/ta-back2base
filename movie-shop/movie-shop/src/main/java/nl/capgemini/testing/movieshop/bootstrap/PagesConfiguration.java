package nl.capgemini.testing.movieshop.bootstrap;

import nl.capgemini.testing.movieshop.model.pages.Page;
import nl.capgemini.testing.movieshop.services.pages.PageService;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

@Configuration
public class PagesConfiguration {

    @EventListener
    public void setupPages(final ContextRefreshedEvent event) {
        final PageService service = event.getApplicationContext().getBean(PageService.class);
        makePage(service, "Known Bugs", "known_bugs");
        makePage(service, "Shutting down", "shutting_down");
    }

    private void makePage(final PageService service, final String title, final String url) {
        final Page result = new Page();
        result.setTitle(title);
        result.setUrl(url);
        service.createPage(result);
    }

}
