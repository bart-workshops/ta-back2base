package nl.capgemini.testing.movieshop.controllers.generic;


import lombok.RequiredArgsConstructor;
import nl.capgemini.testing.movieshop.controllers.users.FrontEndUser;
import nl.capgemini.testing.movieshop.model.users.User;
import nl.capgemini.testing.movieshop.services.menu.MenuAware;
import nl.capgemini.testing.movieshop.services.menu.MenuService;
import nl.capgemini.testing.movieshop.services.users.UserManagementService;
import nl.capgemini.testing.movieshop.services.users.UserRoles;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.Map;
import java.util.stream.Collectors;

@Aspect
@Component
@RequiredArgsConstructor
public class FrameDataInjector {

    public static final String REQUEST_MAPPING_SELECTOR =
            "@annotation(org.springframework.web.bind.annotation.GetMapping) ||" +
                    "@annotation(org.springframework.web.bind.annotation.PostMapping) ||" +
                    "@annotation(org.springframework.web.bind.annotation.RequestMapping)";
    public static final String ROLES_KEY = "__roles";
    private static final User DUMMY_USER;

    static {
        DUMMY_USER = new User();
        DUMMY_USER.setColor("add8e6");
    }

    private final MenuService menuService;
    private final UserManagementService userManagementService;

    @Around(REQUEST_MAPPING_SELECTOR)
    public Object injectData(final ProceedingJoinPoint joinPoint) throws Throwable {
        final Controller mvcControllerAnnotation = AnnotationUtils.findAnnotation(
                joinPoint.getTarget().getClass(),
                Controller.class
        );
        if (mvcControllerAnnotation == null) {
            return joinPoint.proceed();
        }

        final HttpSession session = getSessionFromJoinPoint(joinPoint.getArgs());

        final Object result = joinPoint.proceed();
        return result instanceof ModelAndView
                ? postProcess(joinPoint.getTarget(), session, (ModelAndView) result)
                : result;
    }

    private HttpSession getSessionFromJoinPoint(final Object[] args) {
        for (final Object argument : args) {
            if (argument instanceof HttpSession) {
                return (HttpSession) argument;
            }
        }
        return null;
    }

    public ModelAndView postProcess(final Object controller, final HttpSession session, final ModelAndView result) {
        final Map<String, Object> model = result.getModel();
        addMenu(controller, model);
        processProtectedValues(session, model);
        addLoggedInData(session, model);
        return result;
    }

    private void addMenu(final Object controller, final Map<String, Object> result) {
        if (controller instanceof MenuAware) {
            result.put("__menu", menuService.fillMenu((MenuAware) controller));
        } else {
            result.put("__menu", menuService.fillMenu(null));
        }
    }

    private void processProtectedValues(final HttpSession session, final Map<String, Object> result) {
        final UserRoles[] roles = (UserRoles[]) result.get(ROLES_KEY);
        if (roles != null) {
            result.entrySet().stream()
                    .filter(e -> e.getValue() instanceof ViewBuilder.ProtectedValue)
                    .map(Map.Entry::getKey)
                    .collect(Collectors.toSet())// Create set of keys to release modification lock on model
                    .forEach(k -> processProtectedValue(k, session, result));
        }
    }

    private void processProtectedValue(final String key, final HttpSession session, final Map<String, Object> result) {
        final Object value = ((ViewBuilder.ProtectedValue) result.get(key))
                .get(userManagementService, session);
        if (value == null) {
            result.remove(key);
        } else {
            result.put(key, value);
        }
    }

    private void addLoggedInData(final HttpSession session, final Map<String, Object> result) {
        if (!result.containsKey("user")) {
            addUserDataToMap(session, result);
        }

        final UserRoles[] roles = (UserRoles[]) result.get(ROLES_KEY);
        if (roles != null) {
            result.put("__canedit", userManagementService.hasRoleSession(session, roles));
        }
        result.put("__canlogout", !userManagementService.hasRoleSession(session, UserRoles.GUEST));
        result.remove(ROLES_KEY);
    }

    private void addUserDataToMap(final HttpSession session, final Map<String, Object> result) {
        if (session == null) {
            addUserToresult(result, DUMMY_USER);
            return;
        }
        final User sessionUser = (User) session.getAttribute("userDetails");
        if (sessionUser == null) {
            addUserToresult(result, DUMMY_USER);
        } else {
            addUserToresult(result, sessionUser);
        }
    }

    private void addUserToresult(final Map<String, Object> result, final User dummyUser) {
        result.put("user", new FrontEndUser(dummyUser));
    }

}
