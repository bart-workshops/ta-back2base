package nl.capgemini.testing.movieshop.controllers.validators;

import lombok.RequiredArgsConstructor;
import nl.capgemini.testing.movieshop.controllers.ApiValidator;

@RequiredArgsConstructor
public class BooleanValidator extends ApiValidator {

    private final String trueValue;
    private final String falseValue;

    @Override
    protected ApiValidator doClone() {
        return new BooleanValidator(trueValue, falseValue);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ValidationResult<Boolean> doValidate(final String value) {
        if (trueValue.equalsIgnoreCase(value)) {
            return new ValidationResult<>(true);
        } else if (falseValue.equalsIgnoreCase(value)) {
            return new ValidationResult<>(false);
        }
        return new ValidationResult<>(false,
                String.format("Incorrect boolean value received. Expected %s or %s.", trueValue, falseValue)
        );
    }

}
