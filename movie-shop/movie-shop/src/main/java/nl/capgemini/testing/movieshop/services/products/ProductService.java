package nl.capgemini.testing.movieshop.services.products;

import nl.capgemini.testing.movieshop.controllers.products.ProductRepresentation;
import nl.capgemini.testing.movieshop.model.products.Genre;
import nl.capgemini.testing.movieshop.model.products.Product;
import nl.capgemini.testing.movieshop.repositories.ProductRepository;
import nl.capgemini.testing.movieshop.repositories.SearchContext;
import org.springframework.http.HttpStatus;

import java.util.List;
import java.util.Map;

/**
 * This service encompasses the main catalogue business rules. It pulls data from the {@link ProductRepository} as
 * storage.
 */
public interface ProductService {

    /**
     * Find products based on the given search context. If the search context is missing, a new one will be provides
     * without any filters.
     *
     * @param searchContext Search context to find the different products
     * @return a list of products
     */
    List<Product> retrieveProducts(final SearchContext searchContext);

    void updateProduct(long id, Map<String, Object> body);

    long createProduct();

    List<Genre> retrieveGenres();

    void fillGenres(final String genre, ProductRepresentation productRepresentation);

    void deleteProduct(long id);

    void createException(HttpStatus status, String message);
}
