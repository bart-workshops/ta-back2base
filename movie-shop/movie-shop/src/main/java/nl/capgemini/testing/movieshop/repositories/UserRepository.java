package nl.capgemini.testing.movieshop.repositories;

import nl.capgemini.testing.movieshop.model.users.User;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface UserRepository extends CrudRepository<User, Long> {

    Optional<User> findFirstByUsername(String username);

}
