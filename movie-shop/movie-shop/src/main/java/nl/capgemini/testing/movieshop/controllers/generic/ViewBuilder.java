package nl.capgemini.testing.movieshop.controllers.generic;

import lombok.RequiredArgsConstructor;
import nl.capgemini.testing.movieshop.services.users.UserManagementService;
import nl.capgemini.testing.movieshop.services.users.UserRoles;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * This builder works in conjunction with the frame.ftlh to build up the site lay-out. It makes sure that a number
 * of required values are in the model.
 */
public class ViewBuilder {

    public static final String ERRORS_KEY = "errors";
    public static final String FORMDATA = "formdata";
    private final Map<String, Object> model = new HashMap<>();
    private final HttpSession session;

    /**
     * Start a view and link it to the template with the specified viewName.
     *
     * @param session
     * @param viewName the template name, which can include a path based from the templates folder in the resources
     */
    public ViewBuilder(final HttpSession session, final String viewName) {
        this.session = session;
        model.put("__content", viewName + ".ftlh");
        model.put("__scripts", new ArrayList<>());
        model.put("__styles", new ArrayList<>());
    }

    @SuppressWarnings("unchecked")
    public static List<String> safeGetErrorsFromSession(final HttpSession session) {
        // This method does the same thing as safeGetFormDataFromSession() below, except for returning a list
        final List<String> result = (List<String>) session.getAttribute(ERRORS_KEY);
        session.removeAttribute(ERRORS_KEY);
        return result == null ? Collections.emptyList() : result.stream()
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public static Map<String, String> safeGetFormDataFromSession(final HttpSession session) {
        // This method does the same thing as safeGetErrorsFromSession() below, except for returning a map
        final Map<String, String> temp = (Map<String, String>) session.getAttribute(FORMDATA);
        session.removeAttribute(FORMDATA);
        if (temp == null) {
            return new HashMap<>();
        }
        final Map<String, String> result = new HashMap<>();
        for (final Map.Entry<String, String> entry : temp.entrySet()) {
            if (entry.getKey() != null && entry.getValue() != null) {
                result.put(entry.getKey(), entry.getValue());
            }
        }
        return result;
    }

    /**
     * Add an additional external Javascript script to the head of the page.
     *
     * @param url the full url of the script
     * @return this builder
     */
    @SuppressWarnings("unchecked")
    public ViewBuilder script(final String url) {
        ((List<String>) model.get("__scripts")).add(url);
        return this;
    }

    /**
     * Add an additional stylesheet to the head of the page.
     *
     * @param url the full url of the stylesheet
     * @return this builder
     */
    @SuppressWarnings("unchecked")
    public ViewBuilder style(final String url) {
        ((List<String>) model.get("__styles")).add(url);
        return this;
    }

    public ViewBuilder editable(final UserRoles... roles) {
        if (roles.length > 0) {
            model.put("__roles", roles);
        }
        return this;
    }

    /**
     * Add a value to the model. The value can be referenced in the template by the specified key. Any duplicate key
     * will overwrite the previous value.
     * <p>
     * Note: no key can start with a double underscore (__). That pattern of keys are reserved for internal use.
     *
     * @param key   the identifying key of the value
     * @param value the value to be used in the view template
     * @return this builder
     * @throws IllegalArgumentException when a key starts with a double underscore
     */
    public ViewBuilder addToModel(final String key, final Object value) {
        if (key.startsWith("__")) {
            throw new IllegalArgumentException("Key may not start with '__'.");
        }
        model.put(key, value);
        return this;
    }

    /**
     * This method adds a value to the model, provided the user has the rights to see it. The value is provided by a
     * {@link Supplier} instance as an optimization for expensive values.
     *
     * @param key   the unique key which identifies the value
     * @param value the value provider for this entry
     * @param roles the possible roles a user can have to see this entry
     * @return this view builder
     */
    public ViewBuilder addProtectedToModel(final String key, final Supplier<Object> value, final UserRoles... roles) {
        if (roles.length > 0) {
            addToModel(key, new ProtectedValue(value, roles));
        }
        return this;
    }

    /**
     * Build the actual view and model to be used for this page.
     *
     * @return the view and model as needed by Spring to render the page.
     */
    public ModelAndView build() {
        return new ModelAndView("frame", model);
    }

    @RequiredArgsConstructor
    public static class ProtectedValue {

        private final Supplier<Object> value;
        private final UserRoles[] roles;

        public Object get(final UserManagementService userManagementService, final HttpSession session) {
            if (userManagementService.hasRoleSession(session, roles)) {
                return value.get();
            }
            return null;
        }
    }

}
