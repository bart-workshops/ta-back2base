package nl.capgemini.testing.movieshop.controllers.validators;

import nl.capgemini.testing.movieshop.controllers.ApiValidator;

import java.util.regex.Pattern;

public class RegexValidator extends ApiValidator {

    private final Pattern regex;

    public RegexValidator(final String regex) {
        this.regex = Pattern.compile(regex);
    }

    @Override
    protected ApiValidator doClone() {
        return new RegexValidator(this.regex.pattern());
    }

    @Override
    @SuppressWarnings("unchecked")
    protected ValidationResult<String> doValidate(final String value) {
        if (regex.matcher(value).matches()) {
            return new ValidationResult<>(value);
        }
        return new ValidationResult<>(value,
                getParameterName() + " does not match pattern: " + regex.pattern());
    }

}
