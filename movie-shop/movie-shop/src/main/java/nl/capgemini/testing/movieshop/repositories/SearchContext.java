package nl.capgemini.testing.movieshop.repositories;

import nl.capgemini.testing.movieshop.controllers.validators.ValidationResult;

public interface SearchContext {

    SearchContext withOffSet(ValidationResult<Integer> offSet);

    SearchContext withLimit(ValidationResult<Integer> limit);

    SearchContext withTitle(ValidationResult<String> searchTitle);

    SearchContext withId(ValidationResult<Long> searchId);

    SearchContext withYearBefore(ValidationResult<Integer> yearBefore);

    SearchContext withYearAfter(ValidationResult<Integer> yearAfter);

    boolean hasFilters();

}
