package nl.capgemini.testing.movieshop.controllers.generic;

import lombok.RequiredArgsConstructor;
import nl.capgemini.testing.movieshop.services.menu.MenuAware;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorViewResolver;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestClientResponseException;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.function.IntSupplier;

@ControllerAdvice
@RequiredArgsConstructor
public class ErrorHandler implements ErrorViewResolver {

    private static final int[] specializedErrorCodes = {
            400, 401, 403, 404, 405, 500
    };
    private static final Map<Class<? extends Throwable>, Integer> exceptionErrorCodeMap = new HashMap<>();
    private static final MenuAware menuAware = () -> null;

    static {
        exceptionErrorCodeMap.put(HttpRequestMethodNotSupportedException.class, 405);
    }

    private final FrameDataInjector frameDataInjector;

    @ExceptionHandler(value = {RestClientResponseException.class})
    public Object resolveClientErrorPage(final HttpSession session, final RestClientResponseException exception,
                                         final ServletWebRequest request) {
        if (request.getRequest().getRequestURI().startsWith("/api")) {
            return ResponseEntity
                    .status(getStatusCode(exception))
                    .body(getMessage(exception));
        } else {
            return makeUIError(findErrorCodeOrDefault(exception::getRawStatusCode, 400), session);
        }
    }

    @ExceptionHandler(Throwable.class)
    public Object handleAllErrors(final HttpSession session, final Throwable exception,
                                  final ServletWebRequest request) {
        if (request.getRequest().getRequestURI().startsWith("/api")) {
            return ResponseEntity
                    .status(getStatusCode(exception))
                    .body(getMessage(exception));
        } else {
            final int errorCode = exceptionErrorCodeMap.getOrDefault(exception.getClass(), 500);
            return makeUIError(errorCode, session);
        }
    }

    private String getMessage(final Throwable exception) {
        if (exception instanceof RestClientResponseException) {
            return ((RestClientResponseException) exception).getStatusText();
        } else {
            return "{\"message\":\"" + exception.toString() + "\"}";
        }
    }

    private int getStatusCode(final Throwable exception) {
        if (exception instanceof RestClientResponseException) {
            return ((RestClientResponseException) exception).getRawStatusCode();
        } else {
            return 500;
        }
    }

    private ModelAndView makeUIError(final int errorCode, final HttpSession session) {
        final ModelAndView result = new ViewBuilder(session, "errors/" + errorCode)
                .build();
        frameDataInjector.postProcess(menuAware, session, result);
        return result;
    }

    private int findErrorCodeOrDefault(final IntSupplier codeProvider, final int defaultValue) {
        return Arrays.stream(specializedErrorCodes)
                .filter(sec -> sec == codeProvider.getAsInt())
                .findFirst()
                .orElse(defaultValue);
    }

    @Override
    public ModelAndView resolveErrorView(final HttpServletRequest request,
                                         final HttpStatus status,
                                         final Map<String, Object> model) {
        return makeUIError(findErrorCodeOrDefault(status::value, 404), request.getSession());
    }

}
