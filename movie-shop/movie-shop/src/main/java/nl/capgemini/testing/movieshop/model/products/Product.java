package nl.capgemini.testing.movieshop.model.products;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "PRODUCTS")
@NoArgsConstructor
public class Product {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String title;

    @Column(nullable = false)
    private int releaseYear;

    @Column
    private String description;

    @Column
    private String website;

    @Column
    private String genre;

    @Column
    private String posterName;

    @Column(length = 1024*1024)
    private byte[] posterData;

}
