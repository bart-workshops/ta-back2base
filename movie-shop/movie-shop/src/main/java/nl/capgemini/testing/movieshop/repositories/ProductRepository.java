package nl.capgemini.testing.movieshop.repositories;

import nl.capgemini.testing.movieshop.model.products.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<Product, Long>, ContextAwareRepository<Product> {


}
