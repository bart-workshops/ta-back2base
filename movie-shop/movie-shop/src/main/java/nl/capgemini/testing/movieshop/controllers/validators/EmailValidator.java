package nl.capgemini.testing.movieshop.controllers.validators;

import nl.capgemini.testing.movieshop.controllers.ApiValidator;
import org.apache.commons.validator.routines.DomainValidator;

public class EmailValidator extends ApiValidator {

    private static final org.apache.commons.validator.routines.EmailValidator internalValidator;

    static {
        DomainValidator.updateTLDOverride(DomainValidator.ArrayType.LOCAL_PLUS, new String[]{
                "local",
                "localdomain",         // Also widely used as localhost.localdomain
                "localhost",           // RFC2606 defined
        });
        internalValidator =
                org.apache.commons.validator.routines.EmailValidator.getInstance(true, true);
    }

    @Override
    protected ApiValidator doClone() {
        return new EmailValidator();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected ValidationResult<String> doValidate(final String value) {
        if (internalValidator.isValid(value)) {
            return new ValidationResult<>(value);
        }
        return new ValidationResult<>(value,
                getParameterName() + " does not contain a valid email address.");
    }

}
