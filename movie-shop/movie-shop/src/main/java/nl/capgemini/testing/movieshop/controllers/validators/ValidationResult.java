package nl.capgemini.testing.movieshop.controllers.validators;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.util.StringUtils;

@Getter
@RequiredArgsConstructor
public class ValidationResult<T> {

    private final T value;
    private final String errorMessage;

    public ValidationResult(final T value) {
        this(value, null);
    }

    public boolean isValid() {
        return !StringUtils.hasLength(errorMessage);
    }

}
