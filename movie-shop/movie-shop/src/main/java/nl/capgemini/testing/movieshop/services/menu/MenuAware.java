package nl.capgemini.testing.movieshop.services.menu;

import nl.capgemini.testing.movieshop.model.menu.MenuItem;

/**
 * This interface indicates that a frontend controller is aware of the site menu.
 */
public interface MenuAware {

    /**
     * This method is called by the {@link MenuService} to get the selected menu item.
     *
     * @return the menu item of this controller
     */
    MenuItem getMenuItem();

}
