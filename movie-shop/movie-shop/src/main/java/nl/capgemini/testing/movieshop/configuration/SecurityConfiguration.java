package nl.capgemini.testing.movieshop.configuration;

import nl.capgemini.testing.movieshop.controllers.users.UsersController;
import nl.capgemini.testing.movieshop.repositories.UserRepository;
import nl.capgemini.testing.movieshop.services.users.UserManagementService;
import nl.capgemini.testing.movieshop.services.users.UserManagementServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.ExpressionUrlAuthorizationConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Configuration
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final BCryptPasswordEncoder passwordEncoder;
    private final UserManagementServiceImpl userManagementService;

    public SecurityConfiguration(final UserRepository repository) {
        this.passwordEncoder = new BCryptPasswordEncoder();
        this.userManagementService = new UserManagementServiceImpl(repository, passwordEncoder);
        userManagementService.registerFirstUser("admin", "admin");
    }

    @Bean
    public UserManagementService userManagementService() {
        return userManagementService;
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return passwordEncoder;
    }

    @Override
    protected void configure(final HttpSecurity http) throws Exception {
        final ExpressionUrlAuthorizationConfigurer<HttpSecurity>.ExpressionInterceptUrlRegistry expressionInterceptUrlRegistry = http.authorizeRequests();
        expressionInterceptUrlRegistry
                .antMatchers(UsersController.LOGIN_FORM_URL).permitAll()
                .antMatchers("/users", "/users/").authenticated()
                .antMatchers("/admin", "/admin/").hasAuthority("ADMIN")
                .antMatchers("/actuator").permitAll()
                .and()
                .formLogin()
                .loginPage(UsersController.LOGIN_FORM_URL)
                .successHandler(makeSuccessHandler())
                .and()
                .logout()
                .logoutUrl("/users/logout")
                .logoutSuccessUrl("/")
                .invalidateHttpSession(true)
                .deleteCookies("JSESSIONID");
        configureH2Console(http);
        configureSessionSecurity(http);
    }

    private void configureH2Console(final HttpSecurity http) throws Exception {
        http.authorizeRequests()
                .antMatchers("/h2-console/**").permitAll();
        http.csrf()
                .ignoringAntMatchers("/h2-console/**")
                .ignoringAntMatchers("/api/**");
        http.headers().frameOptions()
                .sameOrigin();
    }

    private void configureSessionSecurity(final HttpSecurity http) throws Exception {
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
                .invalidSessionUrl("/");
        http.sessionManagement()
                .maximumSessions(4)
                .expiredUrl("/")
                .maxSessionsPreventsLogin(true);
    }

    private AuthenticationSuccessHandler makeSuccessHandler() {
        return new SavedRequestAwareAuthenticationSuccessHandler() {
            @Override
            public void onAuthenticationSuccess(
                    final HttpServletRequest request,
                    final HttpServletResponse response,
                    final Authentication authentication
            ) throws ServletException, IOException {
                super.onAuthenticationSuccess(request, response, authentication);
                userManagementService.performLogin(request.getSession(), authentication);
            }
        };
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(userManagementService)
                .passwordEncoder(passwordEncoder);
    }


}
