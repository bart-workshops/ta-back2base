package nl.capgemini.testing.movieshop.controllers.products;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;

import java.util.HashMap;
import java.util.Map;

public class MessageBuilder {

    private static final ObjectMapper mapper = new ObjectMapper();

    private final Map<String, Object> messageObject = new HashMap<>();

    private MessageBuilder() {
        // Utility class, will be instantiated internally
    }

    public static MessageBuilder instance() {
        return new MessageBuilder();
    }

    public static MessageBuilder message(final String message) {
        return new MessageBuilder()
                .addField("message", message);
    }

    public static MessageBuilder error(final String message) {
        return new MessageBuilder()
                .addField("message", message);
    }

    public static MessageBuilder error(final Exception exception) {
        return new MessageBuilder()
                .addField("error", exception.getMessage());
    }

    public MessageBuilder addField(final String key, final String value) {
        messageObject.put(key, value);
        return this;
    }

    @SneakyThrows
    public String build() {
        return mapper.writeValueAsString(messageObject);
    }

}
