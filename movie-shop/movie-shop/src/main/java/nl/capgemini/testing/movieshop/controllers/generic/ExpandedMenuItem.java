package nl.capgemini.testing.movieshop.controllers.generic;

import lombok.Getter;
import nl.capgemini.testing.movieshop.model.menu.MenuItem;

/**
 * This is an expanded view of the {@link MenuItem}, which adds additional fields needed for the template to display
 * each individual item correctly.
 */
@Getter
public class ExpandedMenuItem extends MenuItem {

    /**
     * This property indicates if the menu item is active or not.
     *
     * The value of this property will be set by the {@link nl.capgemini.testing.movieshop.services.menu.MenuService}
     * automatically upon building the menu.
     */
    private final boolean active;

    /**
     * Create a new expanded menu item view object
     *
     * @param menuItem the stored menu item to base the appearance on
     * @param active wether or not this menu item is the active one
     */
    public ExpandedMenuItem(final MenuItem menuItem, final boolean active) {
        super(menuItem.getLabel(), menuItem.getUrl());
        this.active = active;
    }

}
