package nl.capgemini.testing.movieshop.services.users;

import nl.capgemini.testing.movieshop.model.users.User;
import nl.capgemini.testing.movieshop.repositories.UserRepository;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class UserManagementServiceImpl implements UserManagementService, UserDetailsService {

    private final UserRepository repository;
    private final PasswordEncoder passwordEncoder;

    public UserManagementServiceImpl(final UserRepository repository, final PasswordEncoder passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserRoles getRole(final HttpSession session) {
        UserRoles role = UserRoles.GUEST;
        if (session == null) {
            return role;
        }
        role = (UserRoles) session.getAttribute(SESSION_ROLE_KEY);
        if (role == null) {
            final Optional<? extends UserRoles> roleFromRepo = retrieveFromRepository(session);
            if (roleFromRepo.isPresent()) {
                role = roleFromRepo.get();
            }
        }
        return role;
    }

    @Override
    public boolean hasRoleSession(final HttpSession session, final UserRoles... roles) {
        return Arrays.asList(roles).contains(getRole(session));
    }

    @Override
    public void registerFirstUser(final String username, final String password) {
        if (repository.findAll().iterator().hasNext()) {
            throw new IllegalStateException("RegisterFirstUser() may only be called once.");
        }
        repository.save(makeUser(username, password, UserRoles.ADMIN));
    }

    @Override
    public List<User> retrieveAllUsers() {
        final List<User> result = new ArrayList<>();
        repository.findAll().forEach(result::add);
        result.sort(Comparator.comparing(User::getUsername));
        return result;
    }

    @Override
    public Optional<User> findSingleUser(final String username) {
        return repository.findFirstByUsername(username);
    }

    @Override
    public void updateUser(final User user) {
        repository.save(user);
    }

    @Override
    public Optional<User> createNewUser(final String username, final String password) {
        return Optional.of(makeUser(username, password, UserRoles.USER));
    }

    private Optional<? extends UserRoles> retrieveFromRepository(final HttpSession session) {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        final Optional<User> user = repository.findFirstByUsername(authentication.getName());
        if (user.isPresent()) {
            session.setAttribute(SESSION_ROLE_KEY, user.get().getRole());
        } else {
            session.setAttribute(SESSION_ROLE_KEY, UserRoles.GUEST);
        }
        return user.map(User::getRole);
    }

    private User makeUser(final String username, final String password, final UserRoles role) {
        final User result = new User();
        result.setUsername(username);
        updatePassword(result, password);
        result.setRole(role);
        result.setColor("000000");
        return result;
    }

    @Override
    public void updatePassword(final User user, final String password) {
        user.setPassword(password == null ? "" : passwordEncoder.encode(password));
    }

    @Override
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        return repository.findFirstByUsername(username)
                .map(this::makeUserDetails)
                .orElseThrow(() -> new UsernameNotFoundException("Unable to find user: " + username));
    }

    private UserDetails makeUserDetails(final User user) {
        return new org.springframework.security.core.userdetails.User(
                user.getUsername(),
                user.getPassword(),
                true,
                true,
                true,
                true,
                Collections.singleton(new SimpleGrantedAuthority(user.getRole().name()))
        );
    }

    public void performLogin(final HttpSession session, final Authentication authentication) {
        session.setAttribute("userDetails", findSingleUser(authentication.getName()).orElse(null));
        session.setAttribute(SESSION_ROLE_KEY, null);
    }

}
