package nl.capgemini.testing.movieshop.controllers.validators;

import nl.capgemini.testing.movieshop.controllers.ApiValidator;

public class RangeValidator extends NumberValidator {

    private final long min;
    private final long max;

    public RangeValidator(final Class<? extends Number> expectedClass, final long min, final long max) {
        super(expectedClass);
        this.min = min;
        this.max = max;
    }

    @Override
    protected ApiValidator doClone() {
        return new RangeValidator(getExpectedClass(), min, max);
    }

    @Override
    public ValidationResult<Number> doValidate(final Number value) {
        if ((value.longValue() >= min) && (value.longValue() <= max)) {
            return new ValidationResult<>(value);
        } else {
            return new ValidationResult<>(value,
                    getParameterName() + " is outside range <" + min + ", " + max + ">.");
        }
    }

}
