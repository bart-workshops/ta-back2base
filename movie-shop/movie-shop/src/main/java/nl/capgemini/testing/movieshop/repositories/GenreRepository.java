package nl.capgemini.testing.movieshop.repositories;

import nl.capgemini.testing.movieshop.model.products.Genre;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {

}
