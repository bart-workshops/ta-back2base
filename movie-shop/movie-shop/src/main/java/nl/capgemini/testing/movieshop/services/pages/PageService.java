package nl.capgemini.testing.movieshop.services.pages;

import nl.capgemini.testing.movieshop.model.pages.Page;

import java.util.Optional;

public interface PageService {

    Optional<Page> findPage(String normalizedUri);

    String retrieveBody(Page page);

    void createPage(Page result);
}
