package nl.capgemini.testing.movieshop.model.menu;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

/**
 * This DTO class is holds the definition of a menu item.
 */
@Getter
@RequiredArgsConstructor
public class MenuItem {

    private final String label;
    private final String url;

}
