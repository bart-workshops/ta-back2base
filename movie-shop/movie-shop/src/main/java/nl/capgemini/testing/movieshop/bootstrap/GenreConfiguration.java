package nl.capgemini.testing.movieshop.bootstrap;

import nl.capgemini.testing.movieshop.model.products.Genre;
import nl.capgemini.testing.movieshop.repositories.GenreRepository;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

@Configuration
public class GenreConfiguration {

    @EventListener
    public void setupProducts(final ContextRefreshedEvent event) {
        final GenreRepository repository = event.getApplicationContext().getBean(GenreRepository.class);
        makeGenre(repository, "Action");
        makeGenre(repository, "Adventure");
        makeGenre(repository, "Animated");
        makeGenre(repository, "Comedy");
        makeGenre(repository, "Drama");
        makeGenre(repository, "Fantasy");
        makeGenre(repository, "Historical");
        makeGenre(repository, "Horror");
        makeGenre(repository, "Romance");
        makeGenre(repository, "Science fiction");
        makeGenre(repository, "Thriller");
        makeGenre(repository, "Western");
    }

    private void makeGenre(final GenreRepository repository, final String title) {
        final Genre newGenre = new Genre();
        newGenre.setTitle(title);
        repository.saveAndFlush(newGenre);
    }

}
