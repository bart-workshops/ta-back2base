package nl.capgemini.testing.movieshop.controllers.validators;

import nl.capgemini.testing.movieshop.controllers.ApiValidator;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateValidator extends ApiValidator {

    @Override
    protected ApiValidator doClone() {
        return new DateValidator();
    }

    @Override
    @SuppressWarnings("unchecked")
    protected ValidationResult<LocalDate> doValidate(final String value) {
        return doValidate(value, DateTimeFormatter.ISO_LOCAL_DATE);
    }

    private ValidationResult<LocalDate> doValidate(final String value, final DateTimeFormatter formatter) {
        try {
            return new ValidationResult<>(LocalDate.parse(value, formatter));
        } catch (final DateTimeParseException e) {
            return new ValidationResult<>(null, "'" + getParameterName() + "' is not a valid date: '"
                    + value + "'");
        }
    }

}
