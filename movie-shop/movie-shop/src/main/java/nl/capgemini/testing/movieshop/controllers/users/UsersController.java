package nl.capgemini.testing.movieshop.controllers.users;

import lombok.extern.slf4j.Slf4j;
import nl.capgemini.testing.movieshop.controllers.generic.ViewBuilder;
import nl.capgemini.testing.movieshop.model.menu.MenuItem;
import nl.capgemini.testing.movieshop.model.users.User;
import nl.capgemini.testing.movieshop.services.menu.MenuAware;
import nl.capgemini.testing.movieshop.services.users.UserManagementService;
import nl.capgemini.testing.movieshop.services.users.UserRoles;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Controller
@RequestMapping("/users")
public class UsersController extends BaseUserController implements MenuAware {

    public static final String LOGIN_FORM_URL = "/users/login";
    public static final String LOGOUT_FORM_URL = "/users/logout";
    public static final String REGISTER_FORM_URL = "/users/register";
    public static final String UPDATE_FORM_URL = "/users/update";
    public static final String FORM_PROCESSING_URL_KEY = "formProcessingUrl";
    private final MenuItem menuItem = new MenuItem("Users", "/users");

    public UsersController(final UserManagementService userManagementService) {
        super(userManagementService);
    }

    @GetMapping({"", "/"})
    public Object index(final HttpSession session) {
        if (userManagementService.hasRoleSession(session, UserRoles.ADMIN)) {
            return new ViewBuilder(session, "users/list")
                    .addToModel("users", getUserList())
                    .build();
        } else {
            final User userDetails = (User) session.getAttribute("userDetails");
            if (userDetails != null) {
                return new RedirectView("/users/detail/" + userDetails.getUsername());
            } else {
                throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Unauthorized");
            }
        }
    }

    @GetMapping("/detail/{username}")
    public ModelAndView userDetail(final HttpSession session, final @PathVariable("username") String username) {
        final Optional<FrontEndUser> user = doGetSingleUser(session, username);
        if (!user.isPresent()) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND, "User not found");
        }
        final Map<String, String> value = ViewBuilder.safeGetFormDataFromSession(session);
        if (value.isEmpty()) {
            fillDataFromUser(user.get(), value);
        }
        return new ViewBuilder(session, "users/details")
                .addToModel(FORM_PROCESSING_URL_KEY, UPDATE_FORM_URL)
                .addToModel(ViewBuilder.ERRORS_KEY, ViewBuilder.safeGetErrorsFromSession(session))
                .addToModel(ViewBuilder.FORMDATA, value)
                .build();
    }

    private void fillDataFromUser(final FrontEndUser user, final Map<String, String> value) {
        value.put("username", user.getUsername());
        value.put("password", "");
        value.put("confirmPassword", "");
        value.put("watching", determineIfUserIsWatching(user));
        value.put("email", user.getEmail());
        value.put("phonenumber", user.getPhoneNumber());
        value.put("birthdate", user.getBirthdate());
        value.put("userrole", user.getRole().toString());
        value.put("color", user.getBackgroundColor());
    }

    private String determineIfUserIsWatching(final FrontEndUser user) {
        if (user.getWatching() == null) {
            return "no";
        } else if (Boolean.TRUE.equals(user.getWatching())) {
            return "yes";
        }
        return "no";
    }

    @GetMapping("/register")
    public ModelAndView register(final HttpSession session) {
        return new ViewBuilder(session, "users/register")
                .addToModel(FORM_PROCESSING_URL_KEY, REGISTER_FORM_URL)
                .addToModel(ViewBuilder.ERRORS_KEY, ViewBuilder.safeGetErrorsFromSession(session))
                .addToModel(ViewBuilder.FORMDATA, ViewBuilder.safeGetFormDataFromSession(session))
                .build();
    }

    @PostMapping("/register")
    public RedirectView performRegistration(final HttpSession session, @RequestParam final Map<String, String> body) {
        final List<String> errors = validateAndProcessUserForm(body, true, session);
        if (errors.isEmpty()) {
            return new RedirectView("/");
        } else {
            return new RedirectView(REGISTER_FORM_URL);
        }
    }

    @PostMapping("/update")
    public RedirectView update(final HttpSession session, @RequestParam final Map<String, String> body) {
        validateAndProcessUserForm(body, false, session);
        return new RedirectView("/users/detail/" + body.get("username"));
    }

    @GetMapping("/login")
    public ModelAndView loginPage(final HttpSession session) {
        session.setAttribute(UserManagementService.SESSION_ROLE_KEY, null);
        return new ViewBuilder(session, "users/loginform")
                .addToModel("loginProcessingUrl", LOGIN_FORM_URL)
                .build();
    }

    @GetMapping("/logout")
    public ModelAndView logoutPage(final HttpSession session) {
        return new ViewBuilder(session, "users/logoutform")
                .addToModel("logoutProcessingUrl", LOGOUT_FORM_URL)
                .build();
    }

    @Override
    public MenuItem getMenuItem() {
        return menuItem;
    }

}
