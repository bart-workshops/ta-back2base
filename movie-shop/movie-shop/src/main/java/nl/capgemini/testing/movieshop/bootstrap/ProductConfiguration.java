package nl.capgemini.testing.movieshop.bootstrap;

import lombok.extern.slf4j.Slf4j;
import nl.capgemini.testing.movieshop.configuration.ClassPathReader;
import nl.capgemini.testing.movieshop.services.products.ProductService;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Configuration
public class ProductConfiguration {

    private final ClassPathReader classPathReader = new ClassPathReader()
            .withBasePath("/bootstrap/");

    @EventListener
    public void setupProducts(final ContextRefreshedEvent event) {
        final ProductService service = event.getApplicationContext().getBean(ProductService.class);
        makeproduct(service, "Alien", 1979, "A movie about aliens", "8,10,11", "alien.jpg");
        makeproduct(service, "The Walk", 2015,
                "A movie about the tightrope walker Philippe Petit", "1,5,7,9", "thewalk.jpg");
        makeproduct(service, "Elite Killer", 2011,
                "A movie with Jason Stratham", "1,2,11", null);
        makeproduct(service, "Robocop", 1987, "", "", "robocop2.jpg");
    }

    private void makeproduct(final ProductService service, final String title, final int year,
                             final String description, final String genre, final String poster) {
        final long id = service.createProduct();
        final Map<String, Object> result = new HashMap<>();
        result.put("title", title);
        result.put("releaseYear", Integer.toString(year));
        result.put("description", description);
        result.put("genre", genre);
        if (StringUtils.hasText(poster)) {
            result.put("poster_name", poster);
            if (!"robocop2.jpg".equals(poster)) {
                result.put("poster_data", classPathReader.readRaw(poster));
            }
        }
        service.updateProduct(id, result);
    }

}
