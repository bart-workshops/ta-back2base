package nl.capgemini.testing.movieshop.controllers.validators;

public class PhoneNumberValidator extends RegexValidator {

    public PhoneNumberValidator() {
        super("[\\+0][0-9\\-\\(\\)]{7,20}");
    }

}
