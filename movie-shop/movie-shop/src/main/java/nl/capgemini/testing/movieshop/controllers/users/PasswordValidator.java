package nl.capgemini.testing.movieshop.controllers.users;

import nl.capgemini.testing.movieshop.controllers.validators.StringLengthValidator;
import nl.capgemini.testing.movieshop.controllers.validators.ValidationResult;

import java.util.Map;

public class PasswordValidator extends StringLengthValidator {

    public PasswordValidator(final int minLength, final int maxLength) {
        super(minLength, maxLength);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> ValidationResult<T> validate(final String paramName, final Map<String, String> formBody) {
        final ValidationResult<T> validPassword = validate(paramName, formBody.get(paramName));
        final ValidationResult<T> validConfirm = validate("confirmPassword", formBody.get("confirmPassword"));
        if (!validPassword.isValid()) {
            return validPassword;
        }
        if (!validConfirm.isValid()) {
            return validConfirm;
        }
        if (validPassword.getValue() == null ^ validConfirm.getValue() == null) {
            return (ValidationResult<T>) new ValidationResult<>(formBody.get(paramName),
                    "Password and confirm password do not match.");
        }
        if (validPassword.getValue() != null && !validPassword.getValue().equals(validConfirm.getValue())) {
            return (ValidationResult<T>) new ValidationResult<>(formBody.get(paramName),
                    "Password and confirm password do not match.");
        }
        return validPassword;
    }

}
