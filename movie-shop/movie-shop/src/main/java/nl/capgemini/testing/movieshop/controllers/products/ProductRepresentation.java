package nl.capgemini.testing.movieshop.controllers.products;

import lombok.Getter;
import lombok.Setter;
import nl.capgemini.testing.movieshop.model.products.Product;

/**
 * This DTO class is representation of a product, which is sent to the template to be shown. The choice to create this
 * class is so that there are 2 different security domains: 1 internal, 1 external, which can have differing
 * information.
 */
@Getter
@Setter
public class ProductRepresentation {

    private final Long id;
    private final String title;
    private final int releaseYear;
    private final String description;
    private String[] genre = new String[0];
    private final String posterName;
    private final String website;

    public ProductRepresentation(final Product product) {
        id = product.getId();
        title = product.getTitle();
        releaseYear = product.getReleaseYear();
        description = product.getDescription();
        posterName = product.getPosterName();
        website = product.getWebsite();
    }

}
