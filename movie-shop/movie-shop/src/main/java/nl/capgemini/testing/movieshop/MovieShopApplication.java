package nl.capgemini.testing.movieshop;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

@Slf4j
@SpringBootApplication
public class MovieShopApplication {

	public static void main(String[] args) {
		final ConfigurableApplicationContext context = SpringApplication.run(MovieShopApplication.class, args);


		log.info("");
		log.info("");
		log.info("");
		log.info("===============================================================");
		log.info("=                                                             =");
		log.info("=    The website can be found on http://localhost:{}        =",
				context.getEnvironment().getProperty("local.server.port")
		);
		log.info("=    The application is now started. Press CTRL+C to stop.    =");
		log.info("=                                                             =");
		log.info("===============================================================");
		log.info("");
		log.info("");
		log.info("");
	}

}
