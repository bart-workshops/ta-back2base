package nl.capgemini.testing.movieshop.controllers.products;

import lombok.extern.slf4j.Slf4j;
import nl.capgemini.testing.movieshop.controllers.validators.NumberValidator;
import nl.capgemini.testing.movieshop.controllers.validators.RangeValidator;
import nl.capgemini.testing.movieshop.controllers.validators.StringLengthValidator;
import nl.capgemini.testing.movieshop.model.products.Genre;
import nl.capgemini.testing.movieshop.model.products.Product;
import nl.capgemini.testing.movieshop.repositories.SearchContext;
import nl.capgemini.testing.movieshop.repositories.impl.SearchContextImpl;
import nl.capgemini.testing.movieshop.services.products.ProductService;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.client.HttpClientErrorException;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Slf4j
public abstract class BaseProductController {

    private static final byte[] MISSING_POSTER;

    static {
        final ClassPathResource resource = new ClassPathResource("/bootstrap/missing.jpg");
        byte[] temp;
        try {
            temp = Files.readAllBytes(resource.getFile().toPath());
        } catch (IOException e) {
            temp = null;
        }
        MISSING_POSTER = temp;
    }

    private final ProductService productService;
    private final RangeValidator yearValidator = new RangeValidator(Integer.class, 1800, 3000);
    private final StringLengthValidator titleValidator = new StringLengthValidator(0, 50);
    private final NumberValidator idValidator = new NumberValidator(Long.class);
    private final NumberValidator integerValidator = new NumberValidator(Integer.class);

    protected BaseProductController(final ProductService productService) {
        this.productService = productService;
    }

    protected List<ProductRepresentation> doListProducts(
            final String searchTitle,
            final String searchId,
            final String yearBefore,
            final String yearAfter,
            final String offset,
            final String limit
    ) {
        final SearchContext searchContext = new SearchContextImpl()
                .withTitle(titleValidator.validate("title", searchTitle))
                .withId(idValidator.validate("searchId", searchId))
                .withYearBefore(yearValidator.validate("year_before", yearBefore))
                .withYearAfter(yearValidator.validate("year_after", yearAfter))
                .withOffSet(integerValidator.validate("offset", offset))
                .withLimit(integerValidator.validate("limit", limit));
        return represent(productService.retrieveProducts(searchContext));
    }

    protected List<ProductRepresentation> doListProducts(final SearchContext searchContext) {
        return represent(productService.retrieveProducts(searchContext));
    }

    protected ProductRepresentation doRetrieveSingleProduct(
            final String id,
            Supplier<ProductRepresentation> noneFoundErrorHandler
    ) {
        final Product product = doRetrieveProductById(id);
        if (product == null) {
            return noneFoundErrorHandler.get();
        }
        return represent(Collections.singletonList(product)).get(0);
    }

    private Product doRetrieveProductById(final String id) {
        final SearchContext searchContext = new SearchContextImpl()
                .withId(idValidator.validate("searchId", id));
        if (!searchContext.hasFilters()) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }
        final List<Product> products = productService.retrieveProducts(searchContext);
        if (products.isEmpty()) {
            return null;
        } else {
            return products.get(0);
        }
    }

    protected long doCreateNew() {
        return productService.createProduct();
    }

    protected void doUpdateProduct(final String idString, final Map<String, Object> body) {
        try {
            final long id = Long.parseLong(idString);
            productService.updateProduct(id, body);
        } catch (final NumberFormatException e) {
            productService.createException(HttpStatus.BAD_REQUEST, "Id must be a number");
        }
    }

    protected final List<ProductRepresentation> represent(final List<Product> products) {
        return products.stream()
                .map(this::createResentation)
                .collect(Collectors.toList());
    }

    private ProductRepresentation createResentation(final Product product) {
        final ProductRepresentation result = new ProductRepresentation(product);
        productService.fillGenres(product.getGenre(), result);
        return result;
    }

    protected List<Genre> doGetGenres() {
        return productService.retrieveGenres();
    }

    protected byte[] doRetrievePoster(final String id) {
        final Product product = doRetrieveProductById(id);
        if (product == null || product.getPosterData() == null || product.getPosterData().length == 0) {
            return MISSING_POSTER;
        }
        return product.getPosterData();
    }

    protected void doDeleteProduct(final long id) {
        productService.deleteProduct(id);
    }

}
