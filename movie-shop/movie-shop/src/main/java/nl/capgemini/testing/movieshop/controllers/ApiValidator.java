package nl.capgemini.testing.movieshop.controllers;

import lombok.AccessLevel;
import lombok.Getter;
import nl.capgemini.testing.movieshop.controllers.validators.ValidationResult;
import org.springframework.util.StringUtils;

import java.util.Map;

public abstract class ApiValidator {

    @Getter(AccessLevel.PROTECTED)
    private String parameterName;

    public final <T> ValidationResult<T> validate(final String paramName, final String value) {
        if (!StringUtils.hasText(value)) {
            return new ValidationResult<>(null);
        }
        final ApiValidator result = doClone();
        result.parameterName = paramName;
        return result.doValidate(value);
    }

    public <T> ValidationResult<T> validate(final String paramName, final Map<String, String> formBody) {
        return validate(paramName, formBody.get(paramName));
    }

    protected abstract ApiValidator doClone();

    protected abstract <T> ValidationResult<T> doValidate(final String value);
}
