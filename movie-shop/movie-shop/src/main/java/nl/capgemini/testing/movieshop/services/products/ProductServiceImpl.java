package nl.capgemini.testing.movieshop.services.products;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.capgemini.testing.movieshop.controllers.products.ProductRepresentation;
import nl.capgemini.testing.movieshop.model.products.Genre;
import nl.capgemini.testing.movieshop.model.products.Product;
import nl.capgemini.testing.movieshop.repositories.GenreRepository;
import nl.capgemini.testing.movieshop.repositories.ProductRepository;
import nl.capgemini.testing.movieshop.repositories.SearchContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;

import javax.transaction.Transactional;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * {@inheritDoc}
 */
@Slf4j
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    public static final String GENRE_KEY = "genre";
    private final ProductRepository productRepository;
    private final GenreRepository genreRepository;

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Product> retrieveProducts(final SearchContext searchContext) {
        return productRepository.findByContext(searchContext);
    }

    @Override
    public void updateProduct(final long id, final Map<String, Object> body) {
        productRepository.findById(id)
                .ifPresent(product -> this.doUpdate(product, body));
    }

    @Override
    @Transactional
    public long createProduct() {
        final Product entity = new Product();
        entity.setTitle("");
        entity.setDescription("");
        final Product result = productRepository.saveAndFlush(entity);
        result.setTitle("New #" + result.getId());
        productRepository.saveAndFlush(result);
        return result.getId();
    }

    @Override
    public List<Genre> retrieveGenres() {
        final List<Genre> result = genreRepository.findAll();
        result.sort(Comparator.comparing(Genre::getTitle));
        return result;
    }

    @Override
    public void fillGenres(final String genre, final ProductRepresentation productRepresentation) {
        if (StringUtils.hasText(genre)) {
            final String[] genres = Arrays.stream(genre.split(","))
                    .map(gId -> genreRepository.getById(Long.parseLong(gId)))
                    .map(Genre::getTitle)
                    .toArray(String[]::new);
            productRepresentation.setGenre(genres);
        }
    }

    @Override
    public void deleteProduct(final long id) {
        productRepository.deleteById(id);
    }

    @Override
    public void createException(final HttpStatus status, final String message) {
        throw HttpClientErrorException.create(
                status,
                message,
                new HttpHeaders(),
                ("{\"message\":\"" + message + "\"}").getBytes(),
                StandardCharsets.UTF_8
        );
    }

    private void doUpdate(final Product product, final Map<String, Object> body) {
        product.setTitle((String) body.get("title"));
        decodeReleaseYear(body, product);
        product.setDescription((String) body.get("description"));
        product.setWebsite((String) body.get("website"));
        if (body.get(GENRE_KEY) instanceof String) {
            product.setGenre((String) body.get(GENRE_KEY));
        } else if (body.get(GENRE_KEY) instanceof List) {
            final List<String> genreList = (List<String>) body.get(GENRE_KEY);
            product.setGenre(convertGenres(genreList));
        } else {
            log.warn("Unknown type for genre: Ignoring value");
        }
        product.setPosterName((String) body.get("poster_name"));
        updatePosterData(product, body);
        productRepository.saveAndFlush(product);
    }

    private String convertGenres(final List<String> genreList) {
        final Map<String, Long> genreMapping = genreRepository.findAll().stream()
                .collect(Collectors.toMap(g -> g.getTitle().toLowerCase(), Genre::getId));
        return genreList.stream()
                .map(String::toLowerCase)
                .map(name -> genreMapping.getOrDefault(name, -1L))
                .filter(gId -> gId > -1L)
                .map(Objects::toString)
                .collect(Collectors.joining(","));
    }

    private void updatePosterData(final Product product, final Map<String, Object> body) {
        final Object value = body.get("poster_data");
        if (value instanceof byte[]) {
            product.setPosterData((byte[]) value);
        } else if (value != null) {
            log.warn("poster_data wasn't a byte array.");
        }
    }

    private void decodeReleaseYear(final Map<String, Object> body, final Product product) {
        final Object value = body.get("releaseYear");
        if (value instanceof String) {
            product.setReleaseYear(Integer.parseInt((String) value));
        } else if (value instanceof Number) {
            product.setReleaseYear(((Number) value).intValue());
        } else {
            log.warn("releaseYear wasn't a number.");
        }
    }


}
