package nl.capgemini.testing.movieshop.controllers.validators;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.capgemini.testing.movieshop.controllers.ApiValidator;

@SuppressWarnings("unchecked")
@Slf4j
@Getter
@RequiredArgsConstructor
public class NumberValidator extends ApiValidator {

    private final Class<? extends Number> expectedClass;

    @Override
    protected ApiValidator doClone() {
        return new NumberValidator(expectedClass);
    }

    @Override
    protected <T> ValidationResult<T> doValidate(final String value) {
        try {
            if (expectedClass == Byte.class) {
                return (ValidationResult<T>) doValidate(Byte.valueOf(value));
            } else if (expectedClass == Short.class) {
                return (ValidationResult<T>) doValidate(Short.valueOf(value));
            } else if (expectedClass == Integer.class) {
                return (ValidationResult<T>) doValidate(Integer.valueOf(value));
            } else if (expectedClass == Long.class) {
                return (ValidationResult<T>) doValidate(Long.valueOf(value));
            } else if (expectedClass == Float.class) {
                return (ValidationResult<T>) doValidate(Float.valueOf(value));
            } else if (expectedClass == Double.class) {
                return (ValidationResult<T>) doValidate(Double.valueOf(value));
            }
        } catch (final NumberFormatException exception) {
            log.error("{} is not a number.", getParameterName(), exception);
        }
        return (ValidationResult<T>) new ValidationResult<>(0, getParameterName() + " is not a number.");
    }

    protected ValidationResult<Number> doValidate(final Number value) {
        return new ValidationResult<>(value);
    }

}
