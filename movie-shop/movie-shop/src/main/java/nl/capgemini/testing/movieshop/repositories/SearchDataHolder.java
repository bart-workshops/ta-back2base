package nl.capgemini.testing.movieshop.repositories;

import java.util.List;
import java.util.Map;

public interface SearchDataHolder {

    Map<String, Object> getParameters();

    char getEscapeCharacter();

    Integer getOffSet();

    Integer getLimit();

    List<Filter> getFilters();
}
