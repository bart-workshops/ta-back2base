package nl.capgemini.testing.movieshop.services.menu;

import nl.capgemini.testing.movieshop.controllers.generic.ExpandedMenuItem;

import java.util.List;

public interface MenuService {

    List<ExpandedMenuItem> fillMenu(MenuAware selectedComponent);

}
