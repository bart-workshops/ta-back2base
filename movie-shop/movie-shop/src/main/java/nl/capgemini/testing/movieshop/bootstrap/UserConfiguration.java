package nl.capgemini.testing.movieshop.bootstrap;

import nl.capgemini.testing.movieshop.model.users.User;
import nl.capgemini.testing.movieshop.services.users.UserManagementService;
import nl.capgemini.testing.movieshop.services.users.UserRoles;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;

import java.time.LocalDate;

@Configuration
public class UserConfiguration {

    @EventListener
    public void setupUsers(final ContextRefreshedEvent event) {
        final UserManagementService service = event.getApplicationContext().getBean(UserManagementService.class);
        makeUser(service, "editor", false, UserRoles.EDITOR, "000088", LocalDate.now());

    }

    private void makeUser(
            final UserManagementService service,
            final String username,
            final boolean watching,
            final UserRoles role,
            final String color,
            final LocalDate birthday
    ) {
        final User result = service.createNewUser(username, username).get();
        result.setWatching(watching);
        result.setRole(role);
        result.setEmail(username + "@ta.cap.local");
        result.setPhoneNumber("0612345678");
        result.setColor(color);
        result.setBirthdate(birthday);
        service.updateUser(result);
    }

}
