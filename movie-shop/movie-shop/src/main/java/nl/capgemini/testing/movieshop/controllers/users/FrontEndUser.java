package nl.capgemini.testing.movieshop.controllers.users;

import lombok.Getter;
import nl.capgemini.testing.movieshop.model.users.User;
import nl.capgemini.testing.movieshop.services.users.UserRoles;

import java.time.format.DateTimeFormatter;

@Getter
public class FrontEndUser {

    private final Long id;
    private final UserRoles role;
    private final String username;
    private final String email;
    private final String phoneNumber;
    private final String backgroundColor;
    private final String foregroundColor;
    private final String birthdate;
    private final Boolean watching;

    public FrontEndUser(final User user) {
        this.id = user.getId();
        this.role = user.getRole();
        this.username = user.getUsername();
        this.email = user.getEmail();
        this.phoneNumber = user.getPhoneNumber();
        this.backgroundColor = "#" + user.getColor();
        this.foregroundColor = "#" + makeForegroundColor(user.getColor());
        if (user.getBirthdate() == null) {
            this.birthdate = "";
        } else {
            this.birthdate = DateTimeFormatter.ISO_DATE.format(user.getBirthdate());
        }
        this.watching = user.getWatching();
    }

    private String makeForegroundColor(final String color) {
        final int red = Integer.parseInt(color.substring(0, 2), 16);
        final int green = Integer.parseInt(color.substring(2, 4), 16);
        final int blue = Integer.parseInt(color.substring(4, 6), 16);
        if (red > 150 || green > 150 || blue > 150) {
            return "000000";
        } else {
            return "FFFFFF";
        }
    }

}
