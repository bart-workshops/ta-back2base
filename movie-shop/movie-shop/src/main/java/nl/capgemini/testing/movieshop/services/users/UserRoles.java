package nl.capgemini.testing.movieshop.services.users;

public enum UserRoles {

    GUEST,
    USER,
    EDITOR,
    ADMIN

}
