package nl.capgemini.testing.movieshop.repositories.filters;

import lombok.RequiredArgsConstructor;
import nl.capgemini.testing.movieshop.repositories.SearchDataHolder;
import nl.capgemini.testing.movieshop.repositories.Filter;

import java.util.function.UnaryOperator;

public class StringFilter extends Filter {

    private final boolean caseSensitive;
    private final String value;
    private final ComparisonOperator operator;

    public StringFilter(final String fieldName, final String value, final boolean caseSensitive, final ComparisonOperator operator) {
        super(fieldName);
        this.caseSensitive = caseSensitive;
        this.value = value;
        this.operator = operator;
    }

    @Override
    public String createQueryClause(final String parameterName, final SearchDataHolder context) {
        context.getParameters().put(parameterName, operator.parameterExpansion.apply(escape(value, context)));
        final String fieldReference = makeFieldReference("c." + fieldName);
        final String parameterReference = makeFieldReference(":" + parameterName);
        return fieldReference + " " + operator.operatorWord + " " + parameterReference;
    }

    private String escape(final String value, final SearchDataHolder context) {
        return value
                .replace("%", context.getEscapeCharacter() + "%")
                .replace("_", context.getEscapeCharacter() + "_");
    }

    private String makeFieldReference(final String name) {
        return caseSensitive ? name : "LOWER(" + name + ")";
    }

    @RequiredArgsConstructor
    public enum ComparisonOperator {

        EXACT("IS", UnaryOperator.identity()),
        STARTS_WITH("LIKE", s -> s + "%"),
        ENDS_WITH("LIKE", s -> "%" + s),
        CONTAINS("LIKE", s -> "%" + s + "%");

        private final String operatorWord;
        private final UnaryOperator<String> parameterExpansion;

    }

}
