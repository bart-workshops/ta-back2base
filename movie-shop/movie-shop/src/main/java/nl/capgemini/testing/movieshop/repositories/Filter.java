package nl.capgemini.testing.movieshop.repositories;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class Filter {

    protected final String fieldName;

    public abstract String createQueryClause(String parameterName, SearchDataHolder context);

}
