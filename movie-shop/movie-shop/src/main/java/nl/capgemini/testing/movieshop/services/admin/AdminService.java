package nl.capgemini.testing.movieshop.services.admin;

import java.util.List;

public interface AdminService {

    List<String> getButtonTiles();

}
