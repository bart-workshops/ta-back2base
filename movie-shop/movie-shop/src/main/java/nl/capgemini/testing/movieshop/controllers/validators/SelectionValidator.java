package nl.capgemini.testing.movieshop.controllers.validators;

import nl.capgemini.testing.movieshop.controllers.ApiValidator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

public class SelectionValidator extends ApiValidator {

    private final Set<String> validPossibilities;

    private SelectionValidator(final Set<String> validPossibilities) {
        this.validPossibilities = validPossibilities;
    }

    public SelectionValidator(final Object[] values) {
        validPossibilities = Arrays.stream(values)
                .map(Objects::toString)
                .collect(Collectors.toSet());
    }

    @Override
    protected ApiValidator doClone() {
        return new SelectionValidator(new HashSet<>(validPossibilities));
    }

    @Override
    @SuppressWarnings("unchecked")
    protected ValidationResult<String> doValidate(final String value) {
        if (validPossibilities.contains(value)) {
            return new ValidationResult<>(value);
        } else {
            return new ValidationResult<>(value,
                    getParameterName() + " does not contain a valid value.");
        }
    }

}
