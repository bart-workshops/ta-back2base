package nl.capgemini.testing.movieshop.model.users;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nl.capgemini.testing.movieshop.services.users.UserRoles;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;

@Getter
@Setter
@Entity
@Table(name = "USERS")
@NoArgsConstructor
public class User implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true, nullable = false)
    private String username;

    @Column
    private String password;

    @Column(nullable = false)
    private UserRoles role = UserRoles.GUEST;

    @Column
    private String email;

    @Column
    private String phoneNumber;

    @Column
    private String color;

    @Column
    private LocalDate birthdate;

    @Column
    private Boolean watching;

}
