package nl.capgemini.testing.movieshop.controllers.generic.model;

import lombok.Getter;
import nl.capgemini.testing.movieshop.model.pages.Page;

@Getter
public class FrontendPage {

    private final String title;
    private final String body;

    public FrontendPage(final Page page, final String body) {
        this.title = page.getTitle();
        this.body = body;
    }

}
