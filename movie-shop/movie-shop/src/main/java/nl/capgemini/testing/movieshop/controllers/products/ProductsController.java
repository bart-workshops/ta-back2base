package nl.capgemini.testing.movieshop.controllers.products;

import lombok.SneakyThrows;
import nl.capgemini.testing.movieshop.controllers.generic.ViewBuilder;
import nl.capgemini.testing.movieshop.model.menu.MenuItem;
import nl.capgemini.testing.movieshop.services.menu.MenuAware;
import nl.capgemini.testing.movieshop.services.products.ProductService;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static nl.capgemini.testing.movieshop.services.users.UserRoles.ADMIN;
import static nl.capgemini.testing.movieshop.services.users.UserRoles.EDITOR;

/**
 * This is the product catalogus controller, which handles the frontend via freemarker templates.
 */
@Order(1)
@Controller
@RequestMapping("/product")
public class ProductsController extends BaseProductController implements MenuAware {

    private final MenuItem menuItem = new MenuItem("Products", "/product");

    public ProductsController(final ProductService productService) {
        super(productService);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MenuItem getMenuItem() {
        return menuItem;
    }

    /**
     * This HTTP GET method returns a list of products in a view and model for the freemarker.
     * Optional URL parameters are used to filter products in the list.
     *
     * @param searchTitle this parameter will perform a "title contains searchTitle" filtering of the list
     * @return a view and model for the freemarker to show the products page
     */
    @GetMapping({"", "/"})
    public ModelAndView productList(
            final HttpSession session, // Do not remove or the session is not available
            @RequestParam(value = "title", required = false) final String searchTitle,
            @RequestParam(value = "id", required = false) final String searchId,
            @RequestParam(value = "year_before", required = false) final String yearBefore,
            @RequestParam(value = "year_after", required = false) final String yearAfter,
            @RequestParam(value = "offset", required = false) final String offset,
            @RequestParam(value = "limit", required = false) final String limit,
            @RequestParam(value = "edit", required = false) final String edit
    ) {
        final List<ProductRepresentation> result = doListProducts(searchTitle, searchId, yearBefore, yearAfter,
                offset, limit);
        return makeViewBuilder(session, edit, "product/index")
                .addToModel("search_title", searchTitle == null ? "" : searchTitle)
                .addToModel("year_before", yearBefore == null ? "" : yearBefore)
                .addToModel("year_after", yearAfter == null ? "" : yearAfter)
                .addToModel("products", result)
                .build();
    }

    private ViewBuilder makeViewBuilder(final HttpSession session, final String edit, final String viewName) {
        return new ViewBuilder(session, viewName)
                .editable(EDITOR, ADMIN)
                .addProtectedToModel("edit", () -> "yes".equals(edit), EDITOR, ADMIN);
    }

    @PostMapping({"", "/"})
    public RedirectView createNewProduct() {
        final long index = doCreateNew();
        return new RedirectView("/product/" + index + "?edit=yes");
    }

    /**
     * This HTTP GET method will show the details of a single product as specified by the id.
     *
     * @param session the session, which contains the user information
     * @param id      the id of the product
     * @return a single product
     * @throws IllegalStateException when there are more than 1 product for an ID
     */
    @GetMapping("/{id}")
    public ModelAndView productDetail(
            final HttpSession session,
            @PathVariable("id") final String id,
            @RequestParam(value = "edit", required = false) final String edit
    ) {
        final ProductRepresentation result = doRetrieveSingleProduct(id,
                () -> null
        );
        if (result == null) {
            throw new HttpClientErrorException(HttpStatus.NOT_FOUND);
        }
        return makeViewBuilder(session, edit, "product/detail")
                .addToModel("product", result)
                .addToModel("genres", doGetGenres())
                .addToModel(ViewBuilder.ERRORS_KEY, ViewBuilder.safeGetErrorsFromSession(session))
                .build();
    }

    @GetMapping("/{id}/poster")
    @ResponseBody
    public ResponseEntity<byte[]> productPoster(@PathVariable("id") final String id) {
        final byte[] body = doRetrievePoster(id);
        return ResponseEntity.ok(body);
    }

    @PostMapping(value = "/{id}")
    public ModelAndView productEdit(
            final MultipartHttpServletRequest request,
            final HttpSession session,
            @PathVariable("id") final String id) {
        final Map<String, Object> body = new HashMap<>();
        request.getParameterMap().forEach((key, value) -> addValueToBody(body, key, value));
        request.getFileMap().forEach((key, value) -> addFileToBody(body, key, value));
        doUpdateProduct(id, body);
        return productDetail(session, id, null);
    }

    @SneakyThrows
    private void addFileToBody(final Map<String, Object> body, final String key, final MultipartFile value) {
        if ("poster".equals(key)) {
            body.put("poster_data", value.getBytes());
            body.put("poster_name", value.getOriginalFilename());
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    private void addValueToBody(final Map<String, Object> body, final String key, final String[] value) {
        if (key.startsWith("_")) {
            // Do nothing by design
        } else if ("genre".equals(key)) {
            body.put(key, String.join(",", value));
        } else if (value.length == 1) {
            body.put(key, value[0]);
        } else {
            body.put(key, value);
        }
    }

}
