package nl.capgemini.testing.movieshop.controllers.users;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.capgemini.testing.movieshop.controllers.ApiValidator;
import nl.capgemini.testing.movieshop.controllers.generic.ViewBuilder;
import nl.capgemini.testing.movieshop.controllers.validators.BooleanValidator;
import nl.capgemini.testing.movieshop.controllers.validators.DateValidator;
import nl.capgemini.testing.movieshop.controllers.validators.EmailValidator;
import nl.capgemini.testing.movieshop.controllers.validators.PhoneNumberValidator;
import nl.capgemini.testing.movieshop.controllers.validators.RegexValidator;
import nl.capgemini.testing.movieshop.controllers.validators.SelectionValidator;
import nl.capgemini.testing.movieshop.controllers.validators.StringLengthValidator;
import nl.capgemini.testing.movieshop.controllers.validators.ValidationResult;
import nl.capgemini.testing.movieshop.model.users.User;
import nl.capgemini.testing.movieshop.services.users.UserManagementService;
import nl.capgemini.testing.movieshop.services.users.UserRoles;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

@Slf4j
@RequiredArgsConstructor
public abstract class BaseUserController {

    protected final UserManagementService userManagementService;
    private final ApiValidator usernameValidator = new StringLengthValidator(4, 16);
    private final ApiValidator passwordValidator = new PasswordValidator(8, 50);
    private final ApiValidator emailValidator = new EmailValidator();
    private final ApiValidator phoneNumberValidator = new PhoneNumberValidator();
    private final ApiValidator colorValidator = new RegexValidator("#[0-9a-fA-F]{6}");
    private final ApiValidator roleValidator = new SelectionValidator(UserRoles.values());
    private final ApiValidator birthdateValidator = new DateValidator();
    private final ApiValidator watchingValidator = new BooleanValidator("yes", "no");

    @SuppressWarnings("squid:S1168")
    protected List<FrontEndUser> getUserList() {
        final List<User> source = userManagementService.retrieveAllUsers();
        final List<FrontEndUser> result = new ArrayList<>();
        for (final User user : source) {
            result.add(new FrontEndUser(user));
        }
        return result;
/*        return userManagementService.retrieveAllUsers().stream()
                .map(FrontEndUser::new)
                .collect(Collectors.toList());*/
    }

    protected Optional<FrontEndUser> doGetSingleUser(final HttpSession session, final String username) {
        if (StringUtils.hasText(username)) {
            return userManagementService.findSingleUser(username).map(FrontEndUser::new);
        }
        return Optional.of((User) session.getAttribute("userDetails"))
                .map(FrontEndUser::new);
    }


    protected List<String> validateAndProcessUserForm(
            final Map<String, String> body,
            final boolean createNew,
            final HttpSession session
    ) {
        final List<String> errorMessages = new ArrayList<>();
        final User user = validateAndGetUser(body, createNew, errorMessages);
        if (!createNew) {
            validateAndSetProperty("role", body, roleValidator,
                    errorMessages, s -> user.setRole(UserRoles.valueOf(s)));
        }
        validateAndSetProperty("email", body, emailValidator,
                errorMessages, user::setEmail);
        validateAndSetProperty("phonenumber", body, phoneNumberValidator,
                errorMessages, user::setPhoneNumber);
        validateAndSetProperty("color", body, colorValidator,
                errorMessages, s -> applyColor(s, user));
        validateAndSetProperty("birthdate", body, birthdateValidator,
                errorMessages, b -> fillBirthDate(b, user));
        validateAndSetProperty("watching", body, watchingValidator,
                errorMessages, b -> applyWatching(b, user));
        if (errorMessages.isEmpty()) {
            userManagementService.updateUser(user);
        }
        session.setAttribute(ViewBuilder.ERRORS_KEY, errorMessages);
        session.setAttribute(ViewBuilder.FORMDATA, body);
        return errorMessages;
    }

    private void applyWatching(final String value, final User user) {
        user.setWatching("yes".equalsIgnoreCase(value));
    }


    private void fillBirthDate(final String input, final User user) {
        user.setBirthdate(LocalDate.parse(input, DateTimeFormatter.ISO_DATE));
    }

    private void applyColor(final String colorCode, final User user) {
        user.setColor(colorCode.substring(1));
    }

    private void validateAndSetProperty(final String paramName, final Map<String, String> body,
                                        final ApiValidator validator, final List<String> errorMessages,
                                        final Consumer<String> setterFunction) {
        final ValidationResult<Object> validatedValue = validator.validate(paramName, body);
        if (validatedValue.isValid()) {
            if (validatedValue.getValue() != null) {
                setterFunction.accept(body.get(paramName));
            }
        } else {
            errorMessages.add(validatedValue.getErrorMessage());
            log.warn("Validation failed: {}", validatedValue.getErrorMessage());
        }
    }

    private User validateAndGetUser(final Map<String, String> body, final boolean createNew,
                                    final List<String> errorMessages) {
        final ValidationResult<String> username = usernameValidator.validate("username", body);
        final ValidationResult<String> password = passwordValidator.validate("password", body);
        final Optional<User> result;
        if (!createNew && username.isValid() && password.isValid()) {
            result = userManagementService.findSingleUser(username.getValue());
            if (password.getValue() != null) {
                result.ifPresent(u -> userManagementService.updatePassword(u, password.getValue()));
            }
        } else if (username.isValid() && password.isValid()) {
            result = userManagementService.createNewUser(username.getValue(), password.getValue());
        } else {
            result = Optional.empty();
            errorMessages.add(username.getErrorMessage());
            errorMessages.add(password.getErrorMessage());
        }
        return result.orElseGet(User::new);
    }

}
