package nl.capgemini.testing.movieshop.repositories;

import nl.capgemini.testing.movieshop.model.pages.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PageRepository extends JpaRepository<Page, Long> {

    Optional<Page> findByUrl(String url);

}
