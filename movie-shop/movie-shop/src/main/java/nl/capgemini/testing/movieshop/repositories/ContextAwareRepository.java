package nl.capgemini.testing.movieshop.repositories;

import java.util.List;

public interface ContextAwareRepository<T> {

    List<T> findByContext(SearchContext searchContext);

}
