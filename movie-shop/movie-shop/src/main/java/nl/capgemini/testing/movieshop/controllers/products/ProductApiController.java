package nl.capgemini.testing.movieshop.controllers.products;

import lombok.extern.slf4j.Slf4j;
import nl.capgemini.testing.movieshop.services.products.ProductService;
import org.springframework.core.annotation.Order;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;

import javax.servlet.http.HttpServletRequest;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Map;

@Slf4j
@Order(1)
@RestController
@RequestMapping("/api/product")
public class ProductApiController extends BaseProductController {

    protected ProductApiController(final ProductService productService) {
        super(productService);
    }

    @GetMapping({"", "/"})
    public ResponseEntity<List<ProductRepresentation>> productList(
            final HttpServletRequest request,
            @RequestParam(value = "title", required = false) final String searchTitle,
            @RequestParam(value = "id", required = false) final String searchId,
            @RequestParam(value = "year_before", required = false) final String yearBefore,
            @RequestParam(value = "year_after", required = false) final String yearAfter,
            @RequestParam(value = "offset", required = false) final String offset,
            @RequestParam(value = "limit", required = false) final String limit
    ) {
        final List<ProductRepresentation> result = doListProducts(searchTitle, searchId, yearBefore, yearAfter,
                offset, limit);
        return ResponseEntity.ok(result);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductRepresentation> productDetail(
            final HttpServletRequest request,
            @PathVariable("id") final String id
    ) {
        final ProductRepresentation result = doRetrieveSingleProduct(
                id, () -> null
        );
        if (result == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(result);
    }

    @PostMapping({"", "/"})
    public ResponseEntity<String> createNewProduct(
            final HttpServletRequest request,
            @RequestBody final Map<String, Object> body) {
        log.info("Create new movie request");
        final long index = doCreateNew();
        doUpdateProduct(Long.toString(index), body);
        try {
            return ResponseEntity
                    .created(new URI("/api/product/" + index))
                    .body("{\"message:\":\"Created movie with index #" + index + "\"}");
        } catch (final URISyntaxException e) {
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body("{" +
                            "\"message\":\"Failed to create create movie\"," +
                            "\"error\":\"" + e.getMessage() + "\"," +
                            "}");
        }
    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ProductRepresentation> productEditViaPut(
            final HttpServletRequest request,
            @PathVariable("id") final String id,
            @RequestBody final Map<String, Object> body) {
        if (!"test-api-key".equals(request.getHeader("api-key"))) {
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "{\"message\":\"Unauthorized\"}");
        }
        log.info("Received PUT request for {}", id);
        doUpdateProduct(id, body);
        return productDetail(request, id);
    }

    @DeleteMapping(value = "/{id}")
    public ResponseEntity<String> productDelete(
            final HttpServletRequest request,
            @PathVariable("id") final String idParam) {
        if (!"test-api-key".equals(request.getHeader("api-key"))) {
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "{\"message\":\"Unauthorized\"}");
        }
        try {
            final long id = Long.parseLong(idParam);
            doDeleteProduct(id);
            return ResponseEntity.ok(MessageBuilder.message("ok").build());
        } catch (final NumberFormatException e) {
            log.warn("Invalid id for '{}'.", idParam);
            return ResponseEntity
                    .status(HttpStatus.BAD_REQUEST)
                    .body(MessageBuilder.error("'" + idParam + "' is not a number.").build());
        } catch (final EmptyResultDataAccessException e) {
            log.warn("id not found for '{}'.", idParam);
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(MessageBuilder.error("'" + idParam + "' was not found.").build());
        } catch (final Exception e) {
            log.error("Unknown exception '{}'.", e.getMessage(), e);
            return ResponseEntity
                    .status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .body(MessageBuilder.error(e).build());
        }
    }

}
