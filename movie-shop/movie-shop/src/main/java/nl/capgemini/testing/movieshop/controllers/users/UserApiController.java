package nl.capgemini.testing.movieshop.controllers.users;

import lombok.extern.slf4j.Slf4j;
import nl.capgemini.testing.movieshop.model.users.User;
import nl.capgemini.testing.movieshop.services.users.UserManagementService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api/users")
public class UserApiController extends BaseUserController {

    public UserApiController(final UserManagementService userManagementService) {
        super(userManagementService);
    }

    @GetMapping({"", "/"})
    public ResponseEntity<List<FrontEndUser>> index(final HttpServletRequest request) {
        return ResponseEntity.ok(getUserList());
    }

    @GetMapping("/detail/{username}")
    public ResponseEntity<FrontEndUser> userDetail(
            final HttpServletRequest request,
            final @PathVariable("username") String username
    ) {
        final Optional<User> result = userManagementService.findSingleUser(username);
        if (result.isPresent()) {
            return ResponseEntity.ok(result.map(FrontEndUser::new).get());
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    @PostMapping("/register")
    public ResponseEntity<List<String>> performRegistration(
            final HttpServletRequest request,
            @RequestParam final Map<String, String> body
    ) {
        final List<String> errors = validateAndProcessUserForm(body, true, request.getSession());
        if (errors.isEmpty()) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().body(errors);
    }

    @PostMapping("/update")
    public ResponseEntity<List<String>> update(
            final HttpServletRequest request,
            @RequestParam final Map<String, String> body
    ) {
        final List<String> errors = validateAndProcessUserForm(body, false, request.getSession());
        if (errors.isEmpty()) {
            return ResponseEntity.ok().build();
        }
        return ResponseEntity.badRequest().body(errors);
    }

}
