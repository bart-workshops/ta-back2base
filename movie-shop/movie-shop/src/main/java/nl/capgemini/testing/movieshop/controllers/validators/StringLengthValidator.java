package nl.capgemini.testing.movieshop.controllers.validators;

import lombok.RequiredArgsConstructor;
import nl.capgemini.testing.movieshop.controllers.ApiValidator;

@RequiredArgsConstructor
public class StringLengthValidator extends ApiValidator {

    private final int minLength;
    private final int maxLength;

    @Override
    protected ApiValidator doClone() {
        return new StringLengthValidator(minLength, maxLength);
    }

    @SuppressWarnings("unchecked")
    @Override
    protected ValidationResult<String> doValidate(final String value) {
        if ((value.length() >= minLength) && (value.length() <= maxLength)) {
            return new ValidationResult<>(value);
        } else {
            return new ValidationResult<>(value,
                    getParameterName() + " is outside range <" + minLength + ", " + maxLength + ">.");
        }
    }
}
