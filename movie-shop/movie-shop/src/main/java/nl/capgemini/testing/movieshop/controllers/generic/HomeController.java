package nl.capgemini.testing.movieshop.controllers.generic;

import lombok.RequiredArgsConstructor;
import nl.capgemini.testing.movieshop.controllers.generic.model.FrontendPage;
import nl.capgemini.testing.movieshop.model.menu.MenuItem;
import nl.capgemini.testing.movieshop.services.menu.MenuAware;
import nl.capgemini.testing.movieshop.services.pages.PageService;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * This controller is a generic controller for the home page.
 * <p>
 * The order of this component makes sure that it is the first in the menu.
 */
@Order(0)
@Controller
@RequiredArgsConstructor
public class HomeController implements MenuAware {

    private final MenuItem menuItem = new MenuItem("Home", "/");
    private final PageService pageService;

    /**
     * {@inheritDoc}
     */
    @Override
    public MenuItem getMenuItem() {
        return menuItem;
    }

    /**
     * This method returns the home page of the site.
     *
     * @return the model and view of the home page
     * @param session
     */
    @GetMapping({"", "/"})
    public ModelAndView index(final HttpSession session) {
        return new ViewBuilder(session, "index")
                .build();
    }

    @GetMapping("/page/**")
    public ModelAndView page(final HttpServletRequest request) {
        final String uri = request.getRequestURI();
        final String normalizedUri = uri.substring("/page/".length());
        final FrontendPage page = pageService.findPage(normalizedUri)
                .map(p -> new FrontendPage(p, pageService.retrieveBody(p)))
                .orElseThrow(() ->
                        new HttpClientErrorException(HttpStatus.NOT_FOUND, "Page " + normalizedUri + " not found")
                );
        return new ViewBuilder(request.getSession(), "page")
                .addToModel("page", page)
                .build();
    }


}
