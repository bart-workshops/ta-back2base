package nl.capgemini.testing.movieshop.services.menu;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.capgemini.testing.movieshop.controllers.generic.ExpandedMenuItem;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class MenuServiceImpl implements MenuService {

    private final List<MenuAware> menuAwareComponents = new ArrayList<>();

    @EventListener
    public void fillMenuComponents(final ContextRefreshedEvent event) {
        menuAwareComponents.clear();
        menuAwareComponents.addAll(event.getApplicationContext().getBeansOfType(MenuAware.class).values());
        menuAwareComponents.sort(Comparator.comparingInt(this::getOrder));
    }

    private int getOrder(final MenuAware menuAware) {
        final Order orderAnnotation = menuAware.getClass().getSuperclass().getAnnotation(Order.class);
        if (orderAnnotation == null) {
            return Integer.MAX_VALUE;
        }
        return orderAnnotation.value();
    }

    @Override
    public List<ExpandedMenuItem> fillMenu(final MenuAware selectedComponent) {
        final List<ExpandedMenuItem> result = new ArrayList<>();

        for (final MenuAware component : menuAwareComponents) {
            result.add(expandMenuItem(component, selectedComponent));
        }

        return result;
    }

    private ExpandedMenuItem expandMenuItem(final MenuAware component, final MenuAware selectedComponent) {
        return new ExpandedMenuItem(component.getMenuItem(), selectedComponent == component);
    }

}
