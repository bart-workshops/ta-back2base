package nl.capgemini.testing.movieshop.controllers.admin;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import nl.capgemini.testing.movieshop.controllers.generic.ViewBuilder;
import nl.capgemini.testing.movieshop.services.users.UserManagementService;
import nl.capgemini.testing.movieshop.services.users.UserRoles;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/admin")
@RequiredArgsConstructor
public class AdminController {

    private final UserManagementService userManagementService;
    private final ConfigurableApplicationContext applicationContext;

    @GetMapping({"", "/"})
    public ModelAndView getOverview(final HttpSession session) {
        if (userManagementService.hasRoleSession(session, UserRoles.ADMIN)) {
            return new ViewBuilder(session, "admin/index")
                    .build();
        } else {
            throw new HttpClientErrorException(HttpStatus.UNAUTHORIZED, "Unauthorized");
        }
    }

    @PostMapping("/shutdown")
    public RedirectView shutdown() {
        new Thread(this::startShutdown).start();
        return new RedirectView("/page/shutting_down");
    }

    @SneakyThrows
    void startShutdown() {
        Thread.sleep(1000);
        applicationContext.close();
    }

}
