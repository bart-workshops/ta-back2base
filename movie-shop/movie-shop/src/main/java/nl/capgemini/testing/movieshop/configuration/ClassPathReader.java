package nl.capgemini.testing.movieshop.configuration;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Slf4j
public class ClassPathReader {

    private String basePath;
    private ErrorResponse codeWhenNotFound = (path, e) -> log.error("Error reading: {}", path, e);

    public byte[] readRaw(final String path) {
        final List<byte[]> buffers = readFromClassPath(path);
        return collectBuffers(buffers);
    }

    @SneakyThrows
    private List<byte[]> readFromClassPath(final String path) {
        final ClassPathResource resource = new ClassPathResource(basePath + path);
        final List<byte[]> bufferList = new ArrayList<>();
        try (final InputStream inputStream = resource.getInputStream()) {
            int bytesRead;
            do {
                byte[] buffer = new byte[1024];
                bytesRead = inputStream.read(buffer);
                bufferList.add(buffer);

            } while (bytesRead > 0);
        } catch (final IOException e) {
            codeWhenNotFound.handle(path, e);
        }
        return bufferList;
    }

    private byte[] collectBuffers(final List<byte[]> bufferList) {
        // Calculate total size of the buffer
        final byte[] result = new byte[bufferList.stream().mapToInt(b -> b.length).sum()];
        int position = 0;
        for (final byte[] buffer : bufferList) {
            System.arraycopy(buffer, 0, result, position, buffer.length);
            position += buffer.length;
        }
        return result;
    }

    public ClassPathReader withBasePath(final String basePath) {
        this.basePath = basePath;
        return this;
    }

    public ClassPathReader withFailedToRead(final ErrorResponse codeWhenNotFound) {
        this.codeWhenNotFound = codeWhenNotFound;
        return this;
    }

    public String readString(final String path) {
        return new String(readRaw(path));
    }

    @FunctionalInterface
    public interface ErrorResponse {

        void handle(String path, Exception e) throws Exception;

    }

}
