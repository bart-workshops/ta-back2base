# Capgemini TA Movie Shop

## Download and install

To build and run this project the following applications are needed:

- [IntelliJ community edition](https://www.jetbrains.com/idea/)
> You can also use your own IDE for Java (such as Eclipse), but this guide will assume you are using IntelliJ.
- [Git](https://git-scm.com)
- [Maven](https://maven.apache.org/download.cgi) (Optional if using IntelliJ)
> For more info about installing maven see [these instructions](https://maven.apache.org/install.html)
- [Java JDK](https://www.oracle.com/technetwork/java/javase/downloads/index.html)
> You can choose the latest version.

## Setup

1. Start IntelliJ
2. Chose "Get from Version Control"
3. Enter the following URL: https://gitlab.com/the-ta-crowd/test-automation-bootcamp/movie-shop.git
4. Choose a folder to clone the Git repository to (IntelliJ will automatically suggest one)
5. Press "Clone"
6. Should this be your first IntelliJ project:
    - Navigate to 'File' > 'Project Structure'
    - In the 'Project' tab, select your installed Java version as 'Project SDK'
    - If there is no Java version listed, then select 'Add SDK' > 'JDK' and browse to where Java was installed.

## How to run from IntelliJ

1. In the 'Project' tree, expand the 'movie-shop' > 'src' > 'main' > 'java'. Keep expanding that branch until you see 
    the 'MovieShopApplication' class.
2. Open this in the editor 
3. Run this class as an application. There are several options here:
    - Press the green triangle in the left-hand border
    - Right-click in the file, then select ```Run 'MovieShopApplication'```

### Troubleshooting

Many things can go wrong, while running a complex application. Here are some things you can try:
- When you get the error that classes cannot be found, click on the ```maven``` tool button (usually near the top-right 
    corner of IntelliJ window). Then expand the ```ta-movieshop (root)``` and ```Lifecycle``` branches. Select both 
    ```Clean``` and ```Install``` from the lifecycle list. Finally, right-click and select ```Run maven build```. 
    Wait until you see a "BUILD SUCCESS" message.

## How to run using only maven

Prerequisite: Maven is installed as separate application

1. Build the application by entering ```mvn clean install -P run```

## How to build and run from the command-line

Prerequisite: Maven is installed as separate application

1. Build the application by entering ```mvn clean install -P runnable```
2. Wait until you see a "BUILD SUCCESS" message. This creates a runnable Java application in the ```target``` folder 
    called ```movie-shop-<some version>-RUNNABLE.jar```. This file can be moved to another location if so needed.
3. Run the application by entering ```java -jar movie-shop-<some version>-RUNNABLE.jar``` in the folder where the 
    application is located.

__IMPORTANT!!!!! When you run the application from the "Windows Explorer", "Linux Files" or "Mac Finder", then it will
start, but not show a window. This means that you cannot close it normally for the time being (see Issue #3)__
